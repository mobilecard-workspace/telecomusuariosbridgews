package com.addcel.telecom.usuarios.model.vo;

import java.util.List;

public class MovimientosTarjetaVO {

	private int tipoMovimiento;
	
	private String tarjeta;
	
	private String login;
	
	private String password;
	
	private String tarjetaCifr;
	
	private String vigencia;
	
	private String idTarjetaUsuario;
	
	private List<MovimientosTarjetaVO> listaTarjetas;
	
	private String respuesta;
	
	private int idAplicacion;

	public int getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(int tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTarjetaCifr() {
		return tarjetaCifr;
	}

	public void setTarjetaCifr(String tarjetaCifr) {
		this.tarjetaCifr = tarjetaCifr;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getIdTarjetaUsuario() {
		return idTarjetaUsuario;
	}

	public void setIdTarjetaUsuario(String idTarjetaUsuario) {
		this.idTarjetaUsuario = idTarjetaUsuario;
	}

	public List<MovimientosTarjetaVO> getListaTarjetas() {
		return listaTarjetas;
	}

	public void setListaTarjetas(List<MovimientosTarjetaVO> listaTarjetas) {
		this.listaTarjetas = listaTarjetas;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public int getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

}