package com.addcel.telecom.usuarios.model.vo;

public class ExperianUserVO {

	private String tipoIdUsuario;
	
	private String idUsuario;
	
	private String nitUsuario; 
	
	private String clave;
	
	private String tipoIdCliente;
	
	private String respuesta;
	
	private String idAplicacion;

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNitUsuario() {
		return nitUsuario;
	}

	public void setNitUsuario(String nitUsuario) {
		this.nitUsuario = nitUsuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getTipoIdCliente() {
		return tipoIdCliente;
	}

	public void setTipoIdCliente(String tipoIdCliente) {
		this.tipoIdCliente = tipoIdCliente;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public String getTipoIdUsuario() {
		return tipoIdUsuario;
	}

	public void setTipoIdUsuario(String tipoIdUsuario) {
		this.tipoIdUsuario = tipoIdUsuario;
	}
	
}