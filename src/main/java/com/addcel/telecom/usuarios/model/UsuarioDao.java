package com.addcel.telecom.usuarios.model;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.telecom.usuarios.model.vo.ExperianUserVO;
import com.addcel.telecom.usuarios.model.vo.GemaltoDataVO;
import com.addcel.telecom.usuarios.model.vo.MovimientosTarjetaVO;
import com.addcel.telecom.usuarios.model.vo.TarjetaVO;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.ws.servicios.vo.AbstractVO;
import com.addcel.telecom.usuarios.ws.servicios.vo.ResponseEnroleIngo;
import com.addcel.telecom.usuarios.ws.servicios.vo.TUsuarios;
import com.addcel.telecom.usuarios.ws.servicios.vo.UsuarioRequest;
import com.addcel.telecom.usuarios.ws.servicios.vo.UsuarioVO;
import com.addcel.telecom.usuarios.ws.servicios.vo.parametroVO;

public class UsuarioDao extends SqlMapClientDaoSupport{
		
	private static final Logger LOG = Logger.getLogger(UsuarioDao.class);
	
	public int guardarUsuario(TUsuarios usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_GUARDAR_USUARIO + usuario.getLogin());
			getSqlMapClientTemplate().queryForObject("guardarUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_GUARDAR_USUARIO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_GUARDAR_USUARIO + usuario.getLogin(), e);
		}
		return resp;
	}
        
        public void actualizarUsuarioIdIngo(ResponseEnroleIngo usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_ACTUALIZAR_USUARIO_IDINGO + usuario.getUsrLogin());
			getSqlMapClientTemplate().queryForObject("actualizarUsuarioIdIngo", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_ACTUALIZAR_USUARIO_IDINGO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_ACTUALIZAR_USUARIO_IDINGO + usuario.getUsrLogin(), e);
		}
		//return resp;
	}
	
	public int actualizarUsuario(UsuarioRequest usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_ACTUALIZAR_USUARIO  + usuario.getLogin());
			getSqlMapClientTemplate().queryForObject("actualizarUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_ACTUALIZAR_USUARIO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_ACTUALIZAR_USUARIO + usuario.getLogin(), e);
		}
		return resp;
	}
        
        public int actualizarUsrLogin(TUsuarios usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_ACTUALIZAR_USUARIO  + usuario.getUsrLogin());
            LOG.info("nuevo login : "  + usuario.getNewUsrLogin());
            LOG.info("idioma : "  + usuario.getIdioma());
			getSqlMapClientTemplate().queryForObject("actualizarUsrLogin", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_ACTUALIZAR_USUARIO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_ACTUALIZAR_USUARIO + usuario.getLogin(), e);
		}
		return resp;
	}
        
        public int actualizarNombreUsu(TUsuarios usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_ACTUALIZAR_NOMBRE_USUARIO  + usuario.getUsrLogin());
                        LOG.info("nuevo login : "  + usuario.getNewUsrLogin());
                        LOG.info("idioma : "  + usuario.getIdioma());
			getSqlMapClientTemplate().queryForObject("actualizarNombreUsu", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_ACTUALIZAR_NOMBRE_USUARIO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_ACTUALIZAR_NOMBRE_USUARIO + usuario.getLogin(), e);
		}
		return resp;
	}
        
        public int actualizarPaisUsu(TUsuarios usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_ACTUALIZAR_PAIS_USUARIO  + usuario.getUsrLogin());
                        LOG.info("nuevo login : "  + usuario.getNewUsrLogin());
                        LOG.info("idioma : "  + usuario.getIdioma());
                        LOG.info("pais : "  + usuario.getIdPais());
			getSqlMapClientTemplate().queryForObject("actualizarPaisUsu", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_ACTUALIZAR_PAIS_USUARIO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_ACTUALIZAR_PAIS_USUARIO + usuario.getLogin(), e);
		}
		return resp;
	}
        
        
        
        public int actualizarCorreo(TUsuarios usuario){
		int resp = 0;
		try{
			LOG.info("actualizacion de correo" + usuario.getUsrLogin());
                        LOG.info("nuevo correo : "  + usuario.geteMail());
                        LOG.info("idioma : "  + usuario.getIdioma());
			getSqlMapClientTemplate().queryForObject("actualizarCorreo", usuario);
			LOG.info("actualizacion de correo respuesta :" + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error("error al actualizar el correo" + usuario.getLogin(), e);
		}
		return resp;
	}
        
        public int actualizarTelefono(TUsuarios usuario){
		int resp = 0;
		try{
			LOG.info("actualizacion de telefono" + usuario.getUsrLogin());
                        LOG.info("nuevo telefono : "  + usuario.getTelefonoOriginal());
                        LOG.info("idioma : "  + usuario.getIdioma());
			getSqlMapClientTemplate().queryForObject("actualizarTelefono", usuario);
			LOG.info("actualizacion de telefono respuesta :" + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error("error al actualizar el telefono" + usuario.getLogin(), e);
		}
		return resp;
	}
	
	public int bajaUsuario(UsuarioRequest usuario) {
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_BAJA_USUARIO  + usuario.getLogin());
			getSqlMapClientTemplate().queryForObject("bajaUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_BAJA_USUARIO + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_BAJA_USUARIO + usuario.getLogin(), e);
		}
		return resp;
	}
	
	public int cambiarTarjetaUsuario(UsuarioRequest usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_CAMBIAR_TARJETA  + usuario.getLogin());
			getSqlMapClientTemplate().queryForObject("cambiarTarjetaUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_CAMBIAR_TARJETA + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_CAMBIAR_TARJETA + usuario.getLogin(), e);
		}
		return resp;
	}
	
	
	public int cambiarPasswordUsuario(UsuarioRequest usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_CAMBIAR_PASSWORD  + usuario.getLogin());
                        LOG.info(Constantes.LOG_DAO_CAMBIAR_PASSWORD  + usuario.getPassword());
                        LOG.info(Constantes.LOG_DAO_CAMBIAR_PASSWORD  + usuario.getNewPassword());
                        LOG.info(Constantes.LOG_DAO_CAMBIAR_PASSWORD  + usuario.getIdioma());
			getSqlMapClientTemplate().queryForObject("cambiarPasswordUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_CAMBIAR_PASSWORD + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_CAMBIAR_PASSWORD + usuario.getLogin(), e);
		}
		return resp;
	}
	
	public int resetPasswordUsuario(AbstractVO usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_RESET_PASSWORD  + usuario.getLogin());
			getSqlMapClientTemplate().queryForObject("resetPasswordUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_RESET_PASSWORD + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_RESET_PASSWORD + usuario.getLogin(), e);
		}
		return resp;
	}
	
	public int loginUsuario(UsuarioRequest usuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_LOGIN  + usuario.getLogin());
                        LOG.info("password usuario : "  + usuario.getPassword());
                        LOG.info("idioma usuario : "  + usuario.getIdioma());
			getSqlMapClientTemplate().queryForObject("loginUsuario", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_LOGIN + usuario.getRespuesta());
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_LOGIN + usuario.getLogin(), e);
		}
		return resp;
	}
	
        public String consultaParametros(parametroVO param) {
		String resp = null;
		try{
			LOG.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS);
			resp = (String) getSqlMapClientTemplate().queryForObject("findByParametro", param);
                        LOG.info("respuesta" + param.getResultado());
                        resp = param.getResultado();
			LOG.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS+"[PARAMETROS OBTENIDOS SATISFACTORIAMENTE]");
		}catch(Exception e){
			LOG.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS);
			LOG.error(e);
			resp = Constantes.ERROR_BD_CONSULTAR_PARAMETROS;
		}
                LOG.info("resp : " + resp);
		return resp;
	}
        
        public String consultaeMailUsuario(parametroVO param) {
		String resp = null;
		try{
			LOG.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS);
			resp = (String) getSqlMapClientTemplate().queryForObject("findeMail", param);
                        LOG.info("respuesta" + param.getResultado());
                        resp = param.getResultado();
			LOG.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS+"[E MAIL OBTENIDO SATISFACTORIAMENTE]");
		}catch(Exception e){
			LOG.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS);
			LOG.error(e);
			resp = Constantes.ERROR_BD_CONSULTAR_PARAMETROS;
		}
                LOG.info("resp : " + resp);
		return resp;
	}
        
        public String consultaPassword(parametroVO param) {
		String resp = null;
		try{
			LOG.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS);
			resp = (String) getSqlMapClientTemplate().queryForObject("consultaPassword", param);
                        LOG.info("respuesta" + param.getResultado());
                        resp = param.getResultado();
			LOG.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS+"[PARAMETROS OBTENIDOS SATISFACTORIAMENTE]");
		}catch(Exception e){
			LOG.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PARAMETROS);
			LOG.error(e);
			resp = Constantes.ERROR_BD_CONSULTAR_PARAMETROS;
		}
                LOG.info("resp : " + resp);
		return resp;
	}
        
	public int gemaltoOtp(int idAplicacion){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_GEMALTO_OTP  + idAplicacion);
			resp = (Integer) getSqlMapClientTemplate().queryForObject("gemaltoOtp", idAplicacion);
			LOG.info(Constantes.LOG_RESP_DAO_GEMALTO_OTP + resp);
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_GEMALTO_OTP + idAplicacion, e);
		}
		return resp;
	}
	
	public int usuarioEnroladoGemalto(long idUsuario){
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_USUARIO_ENROL_GEMALTO  + idUsuario);
			resp = (Integer) getSqlMapClientTemplate().queryForObject("usuarioEnroladoGemalto", idUsuario);
			LOG.info(Constantes.LOG_RESP_DAO_USUARIO_ENROL_GEMALTO + resp);
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_USUARIO_ENROL_GEMALTO + idUsuario, e);
		}
		return resp;
	}
	
	public void actualizaUsuarioEnrolamiento(long idUsuario){
		try{
			LOG.info(Constantes.LOG_DAO_ACTUALIZA_ENROLAMIENTO_GEMALTO  + idUsuario);
			getSqlMapClientTemplate().queryForObject("actualizaUsuarioGemalto", idUsuario);
			LOG.info(Constantes.LOG_RESP_DAO_ACTUALIZA_ENROLAMIENTO_GEMALTO + idUsuario);
		}catch(Exception e){
			LOG.error(Constantes.LOG_ERROR_DAO_ACTUALIZA_ENROLAMIENTO_GEMALTO + idUsuario, e);
		}
		return ;
	}
	
	public GemaltoDataVO getDataGemaltoServicio(int idOperacion){
		GemaltoDataVO gemaltoData = new GemaltoDataVO();
		try{
			LOG.info(Constantes.LOG_DAO_CONSULTANDO_DATOS_GEMALTO  + idOperacion);
			gemaltoData.setOperacion(idOperacion);
			getSqlMapClientTemplate().queryForObject("gemaltoServicios", gemaltoData);
			LOG.info(Constantes.LOG_RESP_DAO_CONSULTANDO_DATOS_GEMALTO + gemaltoData.getRespuesta());
		}catch(Exception e){
			gemaltoData = null;
			LOG.error(Constantes.LOG_ERROR_DAO_CONSULTANDO_DATOS_GEMALTO + idOperacion, e);
		}
		return gemaltoData;
	}
	
	@SuppressWarnings("unchecked")
	public List<TUsuarios> getUsuarios(UsuarioRequest usuario){
		List<TUsuarios> listaUsuarios = null;
		try {
			LOG.info(Constantes.LOG_DAO_CONSULTANDO_USUARIO  + usuario.getLogin());
			listaUsuarios = getSqlMapClientTemplate().queryForList("getUsuarios", usuario);
			LOG.info(Constantes.LOG_RESP_DAO_CONSULTANDO_USUARIO + listaUsuarios.size());
		} catch (Exception e) {
			LOG.error(Constantes.LOG_ERROR_DAO_CONSULTANDO_USUARIO + usuario.getLogin(), e);
		}
		return listaUsuarios;
	}

	public int validaDataCredit(int idAplicacion) {
		int resp = 0;
		try{
			LOG.info(Constantes.LOG_DAO_VALIDACION_DATA_CREDIT  + idAplicacion);
			resp = (Integer) getSqlMapClientTemplate().queryForObject("validacionDataCredit", idAplicacion);
			LOG.info(Constantes.LOG_RESP_DAO_VALIDACION_DATA_CREDIT + resp);
		}catch(Exception e){
			resp = 102;
			LOG.error(Constantes.LOG_ERROR_DAO_VALIDACION_DATA_CREDIT + idAplicacion, e);
		}
		return resp;
	}

	/*public ExperianUserVO getDatosExperianUsers(int idAplicacion) {
		ExperianUserVO experianUser = new ExperianUserVO();
		try{
			LOG.info(Constantes.LOG_DAO_CONSULTANDO_DATOS_EXPERIAN  + idAplicacion);
			experianUser.setIdAplicacion(String.valueOf(idAplicacion));
			getSqlMapClientTemplate().queryForObject("experianUsuarios", experianUser);
			LOG.info(Constantes.LOG_RESP_DAO_CONSULTANDO_DATOS_EXPERIAN + experianUser.getRespuesta());
		}catch(Exception e){
			LOG.error(Constantes.LOG_ERROR_DAO_CONSULTANDO_DATOS_EXPERIAN + idAplicacion, e);
		}
		return experianUser;
	}*/

	/*public void loginComercios(UsuarioRequest usuario) {
		try{
			LOG.info(Constantes.LOG_PROCESO_DAO + usuario.getLogin());
			getSqlMapClientTemplate().queryForObject("loginComercios", usuario);
			LOG.info(Constantes.LOG_RESPUESTA_AUTENTICACION_DAO +  usuario.getRespuesta());
		}catch(Exception e){
			LOG.error(Constantes.ERROR_CONSULTADO_LOGIN_COMERCIOS + usuario.getLogin(), e);
		}
	}

	public String procesaTarjetaUsuario(MovimientosTarjetaVO tarjetasVO) {
		String json = null;
		try{
			LOG.info(Constantes.LOG_INICIO_PROCESO_TARJETAS_USUARIO + tarjetasVO.getLogin());
			getSqlMapClientTemplate().queryForObject("procesaTarjetaUsuario", tarjetasVO);
			LOG.info(Constantes.LOG_RESPUESTA_PROCESO_TARJETAS_USUARIO +  tarjetasVO.getRespuesta());
			json = tarjetasVO.getRespuesta();
		}catch(Exception e){
			LOG.error(Constantes.LOG_ERROR_PROCESO_TARJETAS_USUARIO + tarjetasVO.getLogin(), e);
			json = Constantes.JSON_ERROR_PROCESA_TARJETA;
		}	
		return json;
	}

	public TarjetaVO getUsuarioTarjeta(String login) {
		TarjetaVO tarjeta = null;
		try{
			LOG.info(Constantes.LOG_INICIO_TARJETA_ACTIVA_USUARIO + login);
			tarjeta = (TarjetaVO) getSqlMapClientTemplate().queryForObject("getTarjetaActiva", login);
			if(tarjeta != null){
				LOG.info(Constantes.LOG_RESPUESTA_TARJETA_ACTIVA_USUARIO_OK);
			} else {
				LOG.info(Constantes.LOG_RESPUESTA_TARJETA_ACTIVA_USUARIO_NO_ENCONTRADA);
			}
		}catch(Exception e){
			LOG.error(Constantes.LOG_ERROR_TARJETA_ACTIVA_USUARIO + login, e);
			tarjeta = null;
		}	
		return tarjeta;
	}*/
	
}