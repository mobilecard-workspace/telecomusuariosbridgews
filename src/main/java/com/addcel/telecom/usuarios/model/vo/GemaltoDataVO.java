package com.addcel.telecom.usuarios.model.vo;

public class GemaltoDataVO {
	
	private String fromDomainMnemonic;
	
	private String fromUser;
	
	private String toDomainMnemonic;
	
	private String profileName;
	
	private String format;
	
	private String data;
	
	private String channelName;
	
	private String preferredLocale;
	
	private String locale;
	
	private String tokenAlias;
	
	private int operacion;
	
	private String respuesta;

	public String getFromDomainMnemonic() {
		return fromDomainMnemonic;
	}

	public void setFromDomainMnemonic(String fromDomainMnemonic) {
		this.fromDomainMnemonic = fromDomainMnemonic;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getToDomainMnemonic() {
		return toDomainMnemonic;
	}

	public void setToDomainMnemonic(String toDomainMnemonic) {
		this.toDomainMnemonic = toDomainMnemonic;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getPreferredLocale() {
		return preferredLocale;
	}

	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getTokenAlias() {
		return tokenAlias;
	}

	public void setTokenAlias(String tokenAlias) {
		this.tokenAlias = tokenAlias;
	}

	public int getOperacion() {
		return operacion;
	}

	public void setOperacion(int operacion) {
		this.operacion = operacion;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

}
