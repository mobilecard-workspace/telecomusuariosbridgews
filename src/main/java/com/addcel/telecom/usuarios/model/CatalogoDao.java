package com.addcel.telecom.usuarios.model;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.telecom.usuarios.model.vo.AbstractLabelValue;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.google.gson.Gson;

public class CatalogoDao extends SqlMapClientDaoSupport{

	private static final Logger LOGGER = Logger.getLogger(CatalogoDao.class);
	
	private Gson gson = new Gson();
	
	public String consultaTerminos(String idAplicacion) {
		String resp = null;
		try{
			LOGGER.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_TERMINOS);
			resp = (String) getSqlMapClientTemplate().queryForObject("getTerminos", idAplicacion);
			LOGGER.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_TERMINOS+"[TERMINOS OBTENIDOS SATISFACTORIAMENTE]");
		}catch(Exception e){
			LOGGER.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_TERMINOS);
			LOGGER.error(e);
			resp = Constantes.ERROR_BD_CONSULTAR_TERMINOS;
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	public String consultaPais() {
		String resp = null;
		List<AbstractLabelValue> catalogo = null;
		try{
			LOGGER.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PAIS);
			catalogo = (List<AbstractLabelValue>) getSqlMapClientTemplate().queryForList("getCatalogoPais");
			resp = gson.toJson(catalogo);
			LOGGER.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PAIS+"["+resp+"]");
		}catch(Exception e){
			LOGGER.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_PAIS);
			LOGGER.error(e);
			resp = Constantes.ERROR_BD_CONSULTAR_PAISES;
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	public String consultaDocumentos(String idPais) {
		String resp = null;
		List<AbstractLabelValue> catalogo = null;
		try{
			LOGGER.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_DOCUMENTOS);
			catalogo = (List<AbstractLabelValue>) getSqlMapClientTemplate().queryForList("getCatalogoDocumentos", idPais);
			resp = gson.toJson(catalogo);
			LOGGER.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_DOCUMENTOS+"["+resp+"]");
		}catch(Exception e){
			LOGGER.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_DOCUMENTOS);
			LOGGER.error(e);
			resp = Constantes.ERROR_BD_CONSULTAR_DOCUMENTOS;
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	public String consultaGeneros() {
		String resp = null;
		List<AbstractLabelValue> catalogo = null;
		try{
			LOGGER.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_GENEROS);
			catalogo = (List<AbstractLabelValue>) getSqlMapClientTemplate().queryForList("getCatalogoGenero");
			resp = gson.toJson(catalogo);
			LOGGER.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_GENEROS+"["+resp+"]");
		}catch(Exception e){
			LOGGER.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_GENEROS);
			LOGGER.error(e);
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	public String consultaOperador() {
		String resp = null;
		List<AbstractLabelValue> catalogo = null;
		try{
			LOGGER.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_OPERADOR);
			catalogo = (List<AbstractLabelValue>) getSqlMapClientTemplate().queryForList("getCatalogoOperador");
			resp = gson.toJson(catalogo);
			LOGGER.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_OPERADOR+"["+resp+"]");
		}catch(Exception e){
			LOGGER.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_OPERADOR);
			LOGGER.error(e);
		}
		return resp;
	}

	@SuppressWarnings("unchecked")
	public String consultaCiudades() {
		String resp = null;
		List<AbstractLabelValue> catalogo = null;
		try{
			LOGGER.info(Constantes.PROCESO_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_CIUDADES);
			catalogo = (List<AbstractLabelValue>) getSqlMapClientTemplate().queryForList("getCiudades");
			resp = gson.toJson(catalogo);
			LOGGER.info(Constantes.RESPUESTA_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_CIUDADES+"["+resp+"]");
		}catch(Exception e){
			LOGGER.error(Constantes.PROCESO_ERROR_BD+Constantes.LOG_PIPE+Constantes.LOG_WS_CONSULTA_CIUDADES);
			LOGGER.error(e);
		}
		return resp;
	}

}
