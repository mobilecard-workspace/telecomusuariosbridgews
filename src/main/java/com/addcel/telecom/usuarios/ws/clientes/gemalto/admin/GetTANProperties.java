/**
 * GetTANProperties.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class GetTANProperties  implements java.io.Serializable {
    private java.lang.String fromDomainMnemonic;

    private java.lang.String fromUser;

    private java.lang.String toDomainMnemonic;

    private java.lang.String toUser;

    private int tanAssociationId;

    private java.lang.String locale;

    public GetTANProperties() {
    }

    public GetTANProperties(
           java.lang.String fromDomainMnemonic,
           java.lang.String fromUser,
           java.lang.String toDomainMnemonic,
           java.lang.String toUser,
           int tanAssociationId,
           java.lang.String locale) {
           this.fromDomainMnemonic = fromDomainMnemonic;
           this.fromUser = fromUser;
           this.toDomainMnemonic = toDomainMnemonic;
           this.toUser = toUser;
           this.tanAssociationId = tanAssociationId;
           this.locale = locale;
    }


    /**
     * Gets the fromDomainMnemonic value for this GetTANProperties.
     * 
     * @return fromDomainMnemonic
     */
    public java.lang.String getFromDomainMnemonic() {
        return fromDomainMnemonic;
    }


    /**
     * Sets the fromDomainMnemonic value for this GetTANProperties.
     * 
     * @param fromDomainMnemonic
     */
    public void setFromDomainMnemonic(java.lang.String fromDomainMnemonic) {
        this.fromDomainMnemonic = fromDomainMnemonic;
    }


    /**
     * Gets the fromUser value for this GetTANProperties.
     * 
     * @return fromUser
     */
    public java.lang.String getFromUser() {
        return fromUser;
    }


    /**
     * Sets the fromUser value for this GetTANProperties.
     * 
     * @param fromUser
     */
    public void setFromUser(java.lang.String fromUser) {
        this.fromUser = fromUser;
    }


    /**
     * Gets the toDomainMnemonic value for this GetTANProperties.
     * 
     * @return toDomainMnemonic
     */
    public java.lang.String getToDomainMnemonic() {
        return toDomainMnemonic;
    }


    /**
     * Sets the toDomainMnemonic value for this GetTANProperties.
     * 
     * @param toDomainMnemonic
     */
    public void setToDomainMnemonic(java.lang.String toDomainMnemonic) {
        this.toDomainMnemonic = toDomainMnemonic;
    }


    /**
     * Gets the toUser value for this GetTANProperties.
     * 
     * @return toUser
     */
    public java.lang.String getToUser() {
        return toUser;
    }


    /**
     * Sets the toUser value for this GetTANProperties.
     * 
     * @param toUser
     */
    public void setToUser(java.lang.String toUser) {
        this.toUser = toUser;
    }


    /**
     * Gets the tanAssociationId value for this GetTANProperties.
     * 
     * @return tanAssociationId
     */
    public int getTanAssociationId() {
        return tanAssociationId;
    }


    /**
     * Sets the tanAssociationId value for this GetTANProperties.
     * 
     * @param tanAssociationId
     */
    public void setTanAssociationId(int tanAssociationId) {
        this.tanAssociationId = tanAssociationId;
    }


    /**
     * Gets the locale value for this GetTANProperties.
     * 
     * @return locale
     */
    public java.lang.String getLocale() {
        return locale;
    }


    /**
     * Sets the locale value for this GetTANProperties.
     * 
     * @param locale
     */
    public void setLocale(java.lang.String locale) {
        this.locale = locale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTANProperties)) return false;
        GetTANProperties other = (GetTANProperties) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fromDomainMnemonic==null && other.getFromDomainMnemonic()==null) || 
             (this.fromDomainMnemonic!=null &&
              this.fromDomainMnemonic.equals(other.getFromDomainMnemonic()))) &&
            ((this.fromUser==null && other.getFromUser()==null) || 
             (this.fromUser!=null &&
              this.fromUser.equals(other.getFromUser()))) &&
            ((this.toDomainMnemonic==null && other.getToDomainMnemonic()==null) || 
             (this.toDomainMnemonic!=null &&
              this.toDomainMnemonic.equals(other.getToDomainMnemonic()))) &&
            ((this.toUser==null && other.getToUser()==null) || 
             (this.toUser!=null &&
              this.toUser.equals(other.getToUser()))) &&
            this.tanAssociationId == other.getTanAssociationId() &&
            ((this.locale==null && other.getLocale()==null) || 
             (this.locale!=null &&
              this.locale.equals(other.getLocale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFromDomainMnemonic() != null) {
            _hashCode += getFromDomainMnemonic().hashCode();
        }
        if (getFromUser() != null) {
            _hashCode += getFromUser().hashCode();
        }
        if (getToDomainMnemonic() != null) {
            _hashCode += getToDomainMnemonic().hashCode();
        }
        if (getToUser() != null) {
            _hashCode += getToUser().hashCode();
        }
        _hashCode += getTanAssociationId();
        if (getLocale() != null) {
            _hashCode += getLocale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTANProperties.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">GetTANProperties"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromDomainMnemonic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "fromDomainMnemonic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromUser");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "fromUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toDomainMnemonic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "toDomainMnemonic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toUser");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "toUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tanAssociationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "tanAssociationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "locale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
