package com.addcel.telecom.usuarios.ws.clientes.gestionWallet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TarjetaRequest extends BaseRequest {

	private boolean	determinada; 
	private boolean isMobilecard;
	private int	idTarjeta; 
	private long	idUsuario; 
	private String	pan; 
	private int	tipo; 
	private String	vigencia;
	private String codigo;
	private String nombre;
	private String domAmex;
	private String cpAmex;


	public TarjetaRequest() {
		// TODO Auto-generated constructor stub
	}
	
	
	public boolean isMobilecard() {
		return isMobilecard;
	}


	public void setMobilecard(boolean isMobilecard) {
		this.isMobilecard = isMobilecard;
	}


	public String getDomAmex() {
		return domAmex;
	}


	public void setDomAmex(String domAmex) {
		this.domAmex = domAmex;
	}


	public String getCpAmex() {
		return cpAmex;
	}


	public void setCpAmex(String cpAmex) {
		this.cpAmex = cpAmex;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public boolean isDeterminada() {
		return determinada;
	}
	public void setDeterminada(boolean determinada) {
		this.determinada = determinada;
	}
	public int getIdTarjeta() {
		return idTarjeta;
	}
	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
}
