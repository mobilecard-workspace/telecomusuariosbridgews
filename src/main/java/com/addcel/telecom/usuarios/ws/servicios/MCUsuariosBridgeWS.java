package com.addcel.telecom.usuarios.ws.servicios;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.addcel.telecom.usuarios.services.AddcelGestionCatalogosService;
import com.addcel.telecom.usuarios.services.AddcelGestionUsuariosService;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.utils.UtilsService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(serviceName = "MCUsuariosBridgeWS")
public class MCUsuariosBridgeWS {

  private static final Logger LOGGER = LoggerFactory.getLogger(MCUsuariosBridgeWS.class);

  @WebMethod(operationName = "login")
  public String login(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_LOGIN_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.loginUsuario(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_LOGIN_USUARIO);
    return json;
  }
  
  @WebMethod(operationName = "loginEnc")
  public String loginEnc(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_LOGIN_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.loginUsuarioEnc(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_LOGIN_USUARIO);
    return json;
  }

  @WebMethod(operationName = "guardarUsuario")
  public String guardarUsuario(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_GUARDAR_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.guardarUsuario(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_GUARDAR_USUARIO);
    return json;
  }

  @WebMethod(operationName = "actualizarUsuario")
  public String actualizarUsuario(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_ACTUALIZA_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.actualizarUsuario(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_ACTUALIZA_USUARIO);
    return json;
  }

  @WebMethod(operationName = "actualizarNombreUsu")
  public String actualizarNombreUsu(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_ACTUALIZA_NOMBRE_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.actualizarNombreUsu(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_ACTUALIZA_NOMBRE_USUARIO);
    return json;
  }

  @WebMethod(operationName = "actualizarPaisUsu")
  public String actualizarPaisUsu(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_ACTUALIZA_PAIS_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.actualizarPaisUsu(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_ACTUALIZA_PAIS_USUARIO);
    return json;
  }

  @WebMethod(operationName = "actualizarUsrLogin")
  public String actualizarUsrLogin(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_ACTUALIZA_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.actualizarUsrLogin(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_ACTUALIZA_USUARIO);
    return json;
  }

  @WebMethod(operationName = "actualizarCorreo")
  public String actualizarCorreo(@WebParam(name = "json") String json) {
    LOGGER.info("PROCESO DE ACTUALIZACION DE CORREO DE USUARIO");
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.actualizarCorreo(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info("FINALIZACION DE ACTUALIZACION DE CORREO");
    return json;
  }

  @WebMethod(operationName = "actualizarTelefono")
  public String actualizarTelefono(@WebParam(name = "json") String json) {
    LOGGER.info("PROCESO DE ACTUALIZACION DE TELEFONO DE USUARIO");
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.actualizarTelefono(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info("FINALIZACION DE ACTUALIZACION DE TELEFONO");
    return json;
  }

  public String bajaUsuario(String json) {
    LOGGER.info(Constantes.LOG_INICIO_BAJA_USUARIO);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      // json = addColService.bajaUsuario(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_BAJA_USUARIO);
    return json;
  }

  @WebMethod(operationName = "cambiarPassword")
  public String cambiarPassword(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_CAMBIA_PASSWORD);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.cambiarPassword(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_CAMBIA_PASSWORD);
    return json;
  }

  @WebMethod(operationName = "resetPassword")
  public String resetPassword(@WebParam(name = "json") String json) {
    LOGGER.info(Constantes.LOG_INICIO_RESET_PASSWORD);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      json = addColService.resetPassword(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_RESET_PASSWORD);
    return json;
  }

  public String cambiarTarjeta(String json) {
    LOGGER.info(Constantes.LOG_INICIO_CAMBIO_TARJETA);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      // json = addColService.cambiarTarjeta(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_CAMBIO_TARJETA);
    return json;
  }

  public String consultaCatalogos(String json) {
    LOGGER.info(Constantes.LOG_WS_CONSULTA + Constantes.LOG_PIPE
        + Constantes.LOG_WS_CONSULTA_CATALOGOS);
    AddcelGestionCatalogosService catalogoService = null;
    try {
      catalogoService = new AddcelGestionCatalogosService();
      json = catalogoService.consultaCatalogos(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    return json;
  }

  public String procesaTarjetaUsuario(String json) {
    LOGGER.info(Constantes.LOG_INICIO_MOVIMIENTO_TARJETAS);
    AddcelGestionUsuariosService addColService = null;
    try {
      addColService = new AddcelGestionUsuariosService();
      // json = addColService.procesaTarjetaUsuario(json);
    } catch (Exception e) {
      json = UtilsService.getError(100, e);
    }
    LOGGER.info(Constantes.LOG_FIN_MOVIMIENTO_TARJETAS);
    return json;
  }

}
