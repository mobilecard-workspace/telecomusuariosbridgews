/**
 * GetUserProfileResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class GetUserProfileResponse  implements java.io.Serializable {
    private int getUserProfileResult;

    private java.lang.String profiles;

    public GetUserProfileResponse() {
    }

    public GetUserProfileResponse(
           int getUserProfileResult,
           java.lang.String profiles) {
           this.getUserProfileResult = getUserProfileResult;
           this.profiles = profiles;
    }


    /**
     * Gets the getUserProfileResult value for this GetUserProfileResponse.
     * 
     * @return getUserProfileResult
     */
    public int getGetUserProfileResult() {
        return getUserProfileResult;
    }


    /**
     * Sets the getUserProfileResult value for this GetUserProfileResponse.
     * 
     * @param getUserProfileResult
     */
    public void setGetUserProfileResult(int getUserProfileResult) {
        this.getUserProfileResult = getUserProfileResult;
    }


    /**
     * Gets the profiles value for this GetUserProfileResponse.
     * 
     * @return profiles
     */
    public java.lang.String getProfiles() {
        return profiles;
    }


    /**
     * Sets the profiles value for this GetUserProfileResponse.
     * 
     * @param profiles
     */
    public void setProfiles(java.lang.String profiles) {
        this.profiles = profiles;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUserProfileResponse)) return false;
        GetUserProfileResponse other = (GetUserProfileResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.getUserProfileResult == other.getGetUserProfileResult() &&
            ((this.profiles==null && other.getProfiles()==null) || 
             (this.profiles!=null &&
              this.profiles.equals(other.getProfiles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGetUserProfileResult();
        if (getProfiles() != null) {
            _hashCode += getProfiles().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetUserProfileResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">GetUserProfileResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getUserProfileResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "GetUserProfileResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profiles");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "profiles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
