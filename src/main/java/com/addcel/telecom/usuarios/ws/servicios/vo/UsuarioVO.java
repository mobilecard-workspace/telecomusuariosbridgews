package com.addcel.telecom.usuarios.ws.servicios.vo;

public class UsuarioVO extends UsuarioRequest{
	
	private String operadorDesc;
	private String generoDesc;
	private int franquicia;
	private String franquiciaDesc;
	private int idError;
	private String mensajeError;
	private String paisDesc;
	private int status;
	private boolean autoLogin;
	private String cedula;
	private int tipoCedula;
	private String idUsuarioComercio;
	
	private String serialNumberGemalto;
	
	private String activationCodeGemalto;
	
	private String tokenAlias;
	
	private String codigoRespuestaDC;
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	public String getOperadorDesc() {
		return operadorDesc;
	}
	public void setOperadorDesc(String operadorDesc) {
		this.operadorDesc = operadorDesc;
	}
	public String getGeneroDesc() {
		return generoDesc;
	}
	public void setGeneroDesc(String generoDesc) {
		this.generoDesc = generoDesc;
	}
	public int getFranquicia() {
		return franquicia;
	}
	public void setFranquicia(int franquicia) {
		this.franquicia = franquicia;
	}
	public String getFranquiciaDesc() {
		return franquiciaDesc;
	}
	public void setFranquiciaDesc(String franquiciaDesc) {
		this.franquiciaDesc = franquiciaDesc;
	}
	public String getPaisDesc() {
		return paisDesc;
	}
	public void setPaisDesc(String paisDesc) {
		this.paisDesc = paisDesc;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public boolean isAutoLogin() {
		return autoLogin;
	}
	public void setAutoLogin(boolean autoLogin) {
		this.autoLogin = autoLogin;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public int getTipoCedula() {
		return tipoCedula;
	}
	public void setTipoCedula(int tipoCedula) {
		this.tipoCedula = tipoCedula;
	}
	public String getIdUsuarioComercio() {
		return idUsuarioComercio;
	}
	public void setIdUsuarioComercio(String idUsuarioComercio) {
		this.idUsuarioComercio = idUsuarioComercio;
	}
	public String getSerialNumberGemalto() {
		return serialNumberGemalto;
	}
	public void setSerialNumberGemalto(String serialNumberGemalto) {
		this.serialNumberGemalto = serialNumberGemalto;
	}
	public String getActivationCodeGemalto() {
		return activationCodeGemalto;
	}
	public void setActivationCodeGemalto(String activationCodeGemalto) {
		this.activationCodeGemalto = activationCodeGemalto;
	}
	public String getTokenAlias() {
		return tokenAlias;
	}
	public void setTokenAlias(String tokenAlias) {
		this.tokenAlias = tokenAlias;
	}
	public String getCodigoRespuestaDC() {
		return codigoRespuestaDC;
	}
	public void setCodigoRespuestaDC(String codigoRespuestaDC) {
		this.codigoRespuestaDC = codigoRespuestaDC;
	}


}
