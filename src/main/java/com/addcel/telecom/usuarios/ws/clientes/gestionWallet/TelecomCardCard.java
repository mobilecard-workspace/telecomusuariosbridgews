package com.addcel.telecom.usuarios.ws.clientes.gestionWallet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TelecomCardCard {
	
	
	private double	balance;
	private String	date; 
	@SerializedName("estado")
	@Expose
	private boolean	determinada;
	@SerializedName("idTarjeta")
	@Expose
	private int	idTarjeta; 
	private boolean	isMobilecard; 
	private java.util.List<TelecomcardCardMovement>	movements; 
	@SerializedName("nombre")
	@Expose
	private String	nombre;
	@SerializedName("numTarjeta")
	@Expose
	private String	pan; 
	@SerializedName("franquicia")
	@Expose
	private String	tipo; 
	@SerializedName("vigencia")
	@Expose
	private String	vigencia;
	@SerializedName("ct")
	@Expose
	private String codigo;
	@SerializedName("domamex")
	@Expose
	private String domAmex;
	@SerializedName("cpamex")
	@Expose
	private String cpAmex;
	
	
	
	public String getDomAmex() {
		return domAmex;
	}

	public void setDomAmex(String domAmex) {
		this.domAmex = domAmex;
	}

	public String getCpAmex() {
		return cpAmex;
	}

	public void setCpAmex(String cpAmex) {
		this.cpAmex = cpAmex;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public TelecomCardCard() {
		// TODO Auto-generated constructor stub
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public boolean isDeterminada() {
		return determinada;
	}
	public void setDeterminada(boolean determinada) {
		this.determinada = determinada;
	}
	public int getIdTarjeta() {
		return idTarjeta;
	}
	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	public boolean isMobilecard() {
		return isMobilecard;
	}
	public void setMobilecard(boolean isMobilecard) {
		this.isMobilecard = isMobilecard;
	}
	public java.util.List<TelecomcardCardMovement> getMovements() {
		return movements;
	}
	public void setMovements(java.util.List<TelecomcardCardMovement> movements) {
		this.movements = movements;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
	

	
}
