package com.addcel.telecom.usuarios.ws.clientes.mailSenderWS;

public class MailSenderWSProxy implements com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWS {
  private String _endpoint = null;
  private com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWS mailSenderWS = null;
  
  public MailSenderWSProxy() {
    _initMailSenderWSProxy();
  }
  
  public MailSenderWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initMailSenderWSProxy();
  }
  
  private void _initMailSenderWSProxy() {
    try {
      mailSenderWS = (new com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWSServiceLocator()).getMailSenderWS();
      if (mailSenderWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)mailSenderWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)mailSenderWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (mailSenderWS != null)
      ((javax.xml.rpc.Stub)mailSenderWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWS getMailSenderWS() {
    if (mailSenderWS == null)
      _initMailSenderWSProxy();
    return mailSenderWS;
  }
  
  public java.lang.String enviaCorreo(java.lang.String json) throws java.rmi.RemoteException{
    if (mailSenderWS == null)
      _initMailSenderWSProxy();
    return mailSenderWS.enviaCorreo(json);
  }
  
  public java.lang.String envioSMS(java.lang.String json) throws java.rmi.RemoteException{
    if (mailSenderWS == null)
      _initMailSenderWSProxy();
    return mailSenderWS.envioSMS(json);
  }
  
  public java.lang.String enviaCorreoMandrill(java.lang.String json) throws java.rmi.RemoteException{
    if (mailSenderWS == null)
      _initMailSenderWSProxy();
    return mailSenderWS.enviaCorreoMandrill(json);
  }
  
  
}