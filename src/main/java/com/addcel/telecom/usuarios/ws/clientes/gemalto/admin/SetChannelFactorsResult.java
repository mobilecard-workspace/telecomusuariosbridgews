/**
 * SetChannelFactorsResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class SetChannelFactorsResult  extends com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetChannelFactorsResult  implements java.io.Serializable {
    private java.lang.String channelMnemonic;  // attribute

    private java.lang.String providerMnemonic;  // attribute

    private java.lang.String methodId;  // attribute

    public SetChannelFactorsResult() {
    }

    // Simple Types must have a String constructor
    public SetChannelFactorsResult(java.lang.String _value) {
        super(_value);
    }


    /**
     * Gets the channelMnemonic value for this SetChannelFactorsResult.
     * 
     * @return channelMnemonic
     */
    public java.lang.String getChannelMnemonic() {
        return channelMnemonic;
    }


    /**
     * Sets the channelMnemonic value for this SetChannelFactorsResult.
     * 
     * @param channelMnemonic
     */
    public void setChannelMnemonic(java.lang.String channelMnemonic) {
        this.channelMnemonic = channelMnemonic;
    }


    /**
     * Gets the providerMnemonic value for this SetChannelFactorsResult.
     * 
     * @return providerMnemonic
     */
    public java.lang.String getProviderMnemonic() {
        return providerMnemonic;
    }


    /**
     * Sets the providerMnemonic value for this SetChannelFactorsResult.
     * 
     * @param providerMnemonic
     */
    public void setProviderMnemonic(java.lang.String providerMnemonic) {
        this.providerMnemonic = providerMnemonic;
    }


    /**
     * Gets the methodId value for this SetChannelFactorsResult.
     * 
     * @return methodId
     */
    public java.lang.String getMethodId() {
        return methodId;
    }


    /**
     * Sets the methodId value for this SetChannelFactorsResult.
     * 
     * @param methodId
     */
    public void setMethodId(java.lang.String methodId) {
        this.methodId = methodId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetChannelFactorsResult)) return false;
        SetChannelFactorsResult other = (SetChannelFactorsResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.channelMnemonic==null && other.getChannelMnemonic()==null) || 
             (this.channelMnemonic!=null &&
              this.channelMnemonic.equals(other.getChannelMnemonic()))) &&
            ((this.providerMnemonic==null && other.getProviderMnemonic()==null) || 
             (this.providerMnemonic!=null &&
              this.providerMnemonic.equals(other.getProviderMnemonic()))) &&
            ((this.methodId==null && other.getMethodId()==null) || 
             (this.methodId!=null &&
              this.methodId.equals(other.getMethodId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChannelMnemonic() != null) {
            _hashCode += getChannelMnemonic().hashCode();
        }
        if (getProviderMnemonic() != null) {
            _hashCode += getProviderMnemonic().hashCode();
        }
        if (getMethodId() != null) {
            _hashCode += getMethodId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetChannelFactorsResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "SetChannelFactorsResult"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("channelMnemonic");
        attrField.setXmlName(new javax.xml.namespace.QName("", "channelMnemonic"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("providerMnemonic");
        attrField.setXmlName(new javax.xml.namespace.QName("", "providerMnemonic"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("methodId");
        attrField.setXmlName(new javax.xml.namespace.QName("", "methodId"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.SimpleSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.SimpleDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
