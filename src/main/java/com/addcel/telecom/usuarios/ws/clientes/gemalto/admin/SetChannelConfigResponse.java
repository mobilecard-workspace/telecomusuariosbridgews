/**
 * SetChannelConfigResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class SetChannelConfigResponse  implements java.io.Serializable {
    private com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.SetChannelFactorsResult setChannelConfigResult;

    public SetChannelConfigResponse() {
    }

    public SetChannelConfigResponse(
           com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.SetChannelFactorsResult setChannelConfigResult) {
           this.setChannelConfigResult = setChannelConfigResult;
    }


    /**
     * Gets the setChannelConfigResult value for this SetChannelConfigResponse.
     * 
     * @return setChannelConfigResult
     */
    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.SetChannelFactorsResult getSetChannelConfigResult() {
        return setChannelConfigResult;
    }


    /**
     * Sets the setChannelConfigResult value for this SetChannelConfigResponse.
     * 
     * @param setChannelConfigResult
     */
    public void setSetChannelConfigResult(com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.SetChannelFactorsResult setChannelConfigResult) {
        this.setChannelConfigResult = setChannelConfigResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetChannelConfigResponse)) return false;
        SetChannelConfigResponse other = (SetChannelConfigResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.setChannelConfigResult==null && other.getSetChannelConfigResult()==null) || 
             (this.setChannelConfigResult!=null &&
              this.setChannelConfigResult.equals(other.getSetChannelConfigResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSetChannelConfigResult() != null) {
            _hashCode += getSetChannelConfigResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetChannelConfigResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">SetChannelConfigResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("setChannelConfigResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "SetChannelConfigResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "SetChannelFactorsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
