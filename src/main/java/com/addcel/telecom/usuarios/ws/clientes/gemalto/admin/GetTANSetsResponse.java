/**
 * GetTANSetsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class GetTANSetsResponse  implements java.io.Serializable {
    private com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANSetsResponseGetTANSetsResult getTANSetsResult;

    public GetTANSetsResponse() {
    }

    public GetTANSetsResponse(
           com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANSetsResponseGetTANSetsResult getTANSetsResult) {
           this.getTANSetsResult = getTANSetsResult;
    }


    /**
     * Gets the getTANSetsResult value for this GetTANSetsResponse.
     * 
     * @return getTANSetsResult
     */
    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANSetsResponseGetTANSetsResult getGetTANSetsResult() {
        return getTANSetsResult;
    }


    /**
     * Sets the getTANSetsResult value for this GetTANSetsResponse.
     * 
     * @param getTANSetsResult
     */
    public void setGetTANSetsResult(com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANSetsResponseGetTANSetsResult getTANSetsResult) {
        this.getTANSetsResult = getTANSetsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTANSetsResponse)) return false;
        GetTANSetsResponse other = (GetTANSetsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getTANSetsResult==null && other.getGetTANSetsResult()==null) || 
             (this.getTANSetsResult!=null &&
              this.getTANSetsResult.equals(other.getGetTANSetsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetTANSetsResult() != null) {
            _hashCode += getGetTANSetsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTANSetsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">GetTANSetsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTANSetsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "GetTANSetsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">>GetTANSetsResponse>GetTANSetsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
