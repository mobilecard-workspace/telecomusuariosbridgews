/**
 * GetTokenStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class GetTokenStatusResponse  implements java.io.Serializable {
    private int getTokenStatusResult;

    private int tokenStatusId;

    public GetTokenStatusResponse() {
    }

    public GetTokenStatusResponse(
           int getTokenStatusResult,
           int tokenStatusId) {
           this.getTokenStatusResult = getTokenStatusResult;
           this.tokenStatusId = tokenStatusId;
    }


    /**
     * Gets the getTokenStatusResult value for this GetTokenStatusResponse.
     * 
     * @return getTokenStatusResult
     */
    public int getGetTokenStatusResult() {
        return getTokenStatusResult;
    }


    /**
     * Sets the getTokenStatusResult value for this GetTokenStatusResponse.
     * 
     * @param getTokenStatusResult
     */
    public void setGetTokenStatusResult(int getTokenStatusResult) {
        this.getTokenStatusResult = getTokenStatusResult;
    }


    /**
     * Gets the tokenStatusId value for this GetTokenStatusResponse.
     * 
     * @return tokenStatusId
     */
    public int getTokenStatusId() {
        return tokenStatusId;
    }


    /**
     * Sets the tokenStatusId value for this GetTokenStatusResponse.
     * 
     * @param tokenStatusId
     */
    public void setTokenStatusId(int tokenStatusId) {
        this.tokenStatusId = tokenStatusId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTokenStatusResponse)) return false;
        GetTokenStatusResponse other = (GetTokenStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.getTokenStatusResult == other.getGetTokenStatusResult() &&
            this.tokenStatusId == other.getTokenStatusId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGetTokenStatusResult();
        _hashCode += getTokenStatusId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTokenStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">GetTokenStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTokenStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "GetTokenStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tokenStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "tokenStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
