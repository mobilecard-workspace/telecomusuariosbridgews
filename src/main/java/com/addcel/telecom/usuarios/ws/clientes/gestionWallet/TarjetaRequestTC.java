package com.addcel.telecom.usuarios.ws.clientes.gestionWallet;

public class TarjetaRequestTC {

	private long   idUsuario;
	private String direccion;
	private String estado;
	private String ciudad;
	private String cp;
	private String fechaNac; //(09261983)  DDMMYYYY 
	private String govtId;
	private String govtIdIssueDate; //(02072014)MMDDYYY
	private String govtIdExpirationDate; // (03012020)MMDDYYYY
	private String govtIdIssueState;
	private String idioma;
	private String ssn;
	
	
	public TarjetaRequestTC() {
		// TODO Auto-generated constructor stub
	}

	
	public String getIdioma() {
		return idioma;
	}


	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}


	public String getSsn() {
		return ssn;
	}


	public void setSsn(String ssn) {
		this.ssn = ssn;
	}


	public long getIdUsuario() {
		return idUsuario;
	}


	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public String getCp() {
		return cp;
	}


	public void setCp(String cp) {
		this.cp = cp;
	}


	public String getFechaNac() {
		return fechaNac;
	}


	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}


	public String getGovtId() {
		return govtId;
	}


	public void setGovtId(String govtId) {
		this.govtId = govtId;
	}


	public String getGovtIdIssueDate() {
		return govtIdIssueDate;
	}


	public void setGovtIdIssueDate(String govtIdIssueDate) {
		this.govtIdIssueDate = govtIdIssueDate;
	}


	public String getGovtIdExpirationDate() {
		return govtIdExpirationDate;
	}


	public void setGovtIdExpirationDate(String govtIdExpirationDate) {
		this.govtIdExpirationDate = govtIdExpirationDate;
	}


	public String getGovtIdIssueState() {
		return govtIdIssueState;
	}


	public void setGovtIdIssueState(String govtIdIssueState) {
		this.govtIdIssueState = govtIdIssueState;
	}
	

	
}
