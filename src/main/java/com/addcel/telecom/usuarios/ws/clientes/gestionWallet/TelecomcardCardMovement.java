package com.addcel.telecom.usuarios.ws.clientes.gestionWallet;

public class TelecomcardCardMovement {

	private String	date; 
	private int	id; 
	private String	ticket; 
	private double	total;
	
	public TelecomcardCardMovement() {
		// TODO Auto-generated constructor stub
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	
	
}
