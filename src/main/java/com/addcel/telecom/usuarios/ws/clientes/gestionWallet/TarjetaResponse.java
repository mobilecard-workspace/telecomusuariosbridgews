package com.addcel.telecom.usuarios.ws.clientes.gestionWallet;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TarjetaResponse extends McResponse {

	@SerializedName("TelecomCardCard")
	@Expose
	private List<TelecomCardCard>	tarjetas;

	public TarjetaResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public List<TelecomCardCard> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<TelecomCardCard> tarjetas) {
		this.tarjetas = tarjetas;
	}
	
	
}
