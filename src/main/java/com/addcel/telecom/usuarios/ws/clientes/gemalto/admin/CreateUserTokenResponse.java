/**
 * CreateUserTokenResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class CreateUserTokenResponse  implements java.io.Serializable {
    private int createUserTokenResult;

    private java.lang.String serialNumber;

    private java.lang.String activationCode;

    private java.lang.String createdTokenAlias;

    public CreateUserTokenResponse() {
    }

    public CreateUserTokenResponse(
           int createUserTokenResult,
           java.lang.String serialNumber,
           java.lang.String activationCode,
           java.lang.String createdTokenAlias) {
           this.createUserTokenResult = createUserTokenResult;
           this.serialNumber = serialNumber;
           this.activationCode = activationCode;
           this.createdTokenAlias = createdTokenAlias;
    }


    /**
     * Gets the createUserTokenResult value for this CreateUserTokenResponse.
     * 
     * @return createUserTokenResult
     */
    public int getCreateUserTokenResult() {
        return createUserTokenResult;
    }


    /**
     * Sets the createUserTokenResult value for this CreateUserTokenResponse.
     * 
     * @param createUserTokenResult
     */
    public void setCreateUserTokenResult(int createUserTokenResult) {
        this.createUserTokenResult = createUserTokenResult;
    }


    /**
     * Gets the serialNumber value for this CreateUserTokenResponse.
     * 
     * @return serialNumber
     */
    public java.lang.String getSerialNumber() {
        return serialNumber;
    }


    /**
     * Sets the serialNumber value for this CreateUserTokenResponse.
     * 
     * @param serialNumber
     */
    public void setSerialNumber(java.lang.String serialNumber) {
        this.serialNumber = serialNumber;
    }


    /**
     * Gets the activationCode value for this CreateUserTokenResponse.
     * 
     * @return activationCode
     */
    public java.lang.String getActivationCode() {
        return activationCode;
    }


    /**
     * Sets the activationCode value for this CreateUserTokenResponse.
     * 
     * @param activationCode
     */
    public void setActivationCode(java.lang.String activationCode) {
        this.activationCode = activationCode;
    }


    /**
     * Gets the createdTokenAlias value for this CreateUserTokenResponse.
     * 
     * @return createdTokenAlias
     */
    public java.lang.String getCreatedTokenAlias() {
        return createdTokenAlias;
    }


    /**
     * Sets the createdTokenAlias value for this CreateUserTokenResponse.
     * 
     * @param createdTokenAlias
     */
    public void setCreatedTokenAlias(java.lang.String createdTokenAlias) {
        this.createdTokenAlias = createdTokenAlias;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateUserTokenResponse)) return false;
        CreateUserTokenResponse other = (CreateUserTokenResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.createUserTokenResult == other.getCreateUserTokenResult() &&
            ((this.serialNumber==null && other.getSerialNumber()==null) || 
             (this.serialNumber!=null &&
              this.serialNumber.equals(other.getSerialNumber()))) &&
            ((this.activationCode==null && other.getActivationCode()==null) || 
             (this.activationCode!=null &&
              this.activationCode.equals(other.getActivationCode()))) &&
            ((this.createdTokenAlias==null && other.getCreatedTokenAlias()==null) || 
             (this.createdTokenAlias!=null &&
              this.createdTokenAlias.equals(other.getCreatedTokenAlias())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCreateUserTokenResult();
        if (getSerialNumber() != null) {
            _hashCode += getSerialNumber().hashCode();
        }
        if (getActivationCode() != null) {
            _hashCode += getActivationCode().hashCode();
        }
        if (getCreatedTokenAlias() != null) {
            _hashCode += getCreatedTokenAlias().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateUserTokenResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">CreateUserTokenResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createUserTokenResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "CreateUserTokenResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serialNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "serialNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "activationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdTokenAlias");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "createdTokenAlias"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
