/**
 * AdminWS2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public interface AdminWS2 extends javax.xml.rpc.Service {
    public java.lang.String getAdminWS2SoapAddress();

    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap getAdminWS2Soap() throws javax.xml.rpc.ServiceException;

    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap getAdminWS2Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
