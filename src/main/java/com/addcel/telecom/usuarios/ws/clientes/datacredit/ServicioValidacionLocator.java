/**
 * ServicioValidacionLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.datacredit;

public class ServicioValidacionLocator extends org.apache.axis.client.Service implements com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacion {

    public ServicioValidacionLocator() {
    }


    public ServicioValidacionLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ServicioValidacionLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ServicioValidacion
	private java.lang.String ServicioValidacion_address = "http://172.24.14.7:8080/ValidacionWS/services/ServicioValidacion";
    
    public java.lang.String getServicioValidacionAddress() {
        return ServicioValidacion_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ServicioValidacionWSDDServiceName = "ServicioValidacion";

    public java.lang.String getServicioValidacionWSDDServiceName() {
        return ServicioValidacionWSDDServiceName;
    }

    public void setServicioValidacionWSDDServiceName(java.lang.String name) {
        ServicioValidacionWSDDServiceName = name;
    }

    public com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl getServicioValidacion() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ServicioValidacion_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getServicioValidacion(endpoint);
    }

    public com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl getServicioValidacion(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionSoapBindingStub _stub = new com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionSoapBindingStub(portAddress, this);
            _stub.setPortName(getServicioValidacionWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setServicioValidacionEndpointAddress(java.lang.String address) {
        ServicioValidacion_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionSoapBindingStub _stub = new com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionSoapBindingStub(new java.net.URL(ServicioValidacion_address), this);
                _stub.setPortName(getServicioValidacionWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ServicioValidacion".equals(inputPortName)) {
            return getServicioValidacion();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.datacredito.com.co/services/ServicioValidacion", "ServicioValidacion");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.datacredito.com.co/services/ServicioValidacion", "ServicioValidacion"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ServicioValidacion".equals(portName)) {
            setServicioValidacionEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
