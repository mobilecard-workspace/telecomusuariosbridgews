package com.addcel.telecom.usuarios.ws.clientes.gemalto.userRepository;

public class UserRepositoryWSSoapProxy implements com.addcel.telecom.usuarios.ws.clientes.gemalto.userRepository.UserRepositoryWSSoap {
  private String _endpoint = null;
  private com.addcel.telecom.usuarios.ws.clientes.gemalto.userRepository.UserRepositoryWSSoap userRepositoryWSSoap = null;
  
  public UserRepositoryWSSoapProxy() {
    _initUserRepositoryWSSoapProxy();
  }
  
  public UserRepositoryWSSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initUserRepositoryWSSoapProxy();
  }
  
  private void _initUserRepositoryWSSoapProxy() {
    try {
      userRepositoryWSSoap = (new com.addcel.telecom.usuarios.ws.clientes.gemalto.userRepository.UserRepositoryWSLocator()).getUserRepositoryWSSoap();
      if (userRepositoryWSSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)userRepositoryWSSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)userRepositoryWSSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (userRepositoryWSSoap != null)
      ((javax.xml.rpc.Stub)userRepositoryWSSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.userRepository.UserRepositoryWSSoap getUserRepositoryWSSoap() {
    if (userRepositoryWSSoap == null)
      _initUserRepositoryWSSoapProxy();
    return userRepositoryWSSoap;
  }
  
  public java.lang.String getVersion() throws java.rmi.RemoteException{
    if (userRepositoryWSSoap == null)
      _initUserRepositoryWSSoapProxy();
    return userRepositoryWSSoap.getVersion();
  }
  
  public void addUser(java.lang.String userName, java.lang.String displayName, java.lang.String email, java.lang.String phoneCell, java.lang.String password, javax.xml.rpc.holders.BooleanHolder addUserResult, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException{
    if (userRepositoryWSSoap == null)
      _initUserRepositoryWSSoapProxy();
    userRepositoryWSSoap.addUser(userName, displayName, email, phoneCell, password, addUserResult, returnMessage);
  }
  
  public void searchUser(java.lang.String userName, javax.xml.rpc.holders.IntHolder searchUserResult, javax.xml.rpc.holders.StringHolder displayName, javax.xml.rpc.holders.StringHolder email, javax.xml.rpc.holders.StringHolder phoneCell, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException{
    if (userRepositoryWSSoap == null)
      _initUserRepositoryWSSoapProxy();
    userRepositoryWSSoap.searchUser(userName, searchUserResult, displayName, email, phoneCell, returnMessage);
  }
  
  public void authenticate(java.lang.String userName, java.lang.String password, javax.xml.rpc.holders.BooleanHolder authenticateResult, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException{
    if (userRepositoryWSSoap == null)
      _initUserRepositoryWSSoapProxy();
    userRepositoryWSSoap.authenticate(userName, password, authenticateResult, returnMessage);
  }
  
  public void changePassword(java.lang.String userName, java.lang.String oldPassword, java.lang.String newPassword, javax.xml.rpc.holders.BooleanHolder changePasswordResult, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException{
    if (userRepositoryWSSoap == null)
      _initUserRepositoryWSSoapProxy();
    userRepositoryWSSoap.changePassword(userName, oldPassword, newPassword, changePasswordResult, returnMessage);
  }
  
  
}