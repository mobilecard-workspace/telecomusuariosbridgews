/**
 * UserRepositoryWSSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.userRepository;

public interface UserRepositoryWSSoap extends java.rmi.Remote {
    public java.lang.String getVersion() throws java.rmi.RemoteException;
    public void addUser(java.lang.String userName, java.lang.String displayName, java.lang.String email, java.lang.String phoneCell, java.lang.String password, javax.xml.rpc.holders.BooleanHolder addUserResult, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException;
    public void searchUser(java.lang.String userName, javax.xml.rpc.holders.IntHolder searchUserResult, javax.xml.rpc.holders.StringHolder displayName, javax.xml.rpc.holders.StringHolder email, javax.xml.rpc.holders.StringHolder phoneCell, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException;
    public void authenticate(java.lang.String userName, java.lang.String password, javax.xml.rpc.holders.BooleanHolder authenticateResult, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException;
    public void changePassword(java.lang.String userName, java.lang.String oldPassword, java.lang.String newPassword, javax.xml.rpc.holders.BooleanHolder changePasswordResult, javax.xml.rpc.holders.StringHolder returnMessage) throws java.rmi.RemoteException;
}
