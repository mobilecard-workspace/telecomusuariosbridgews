package com.addcel.telecom.usuarios.ws.servicios.vo;

public class UsuarioResponse{
	
	private long idUsuario;
	
	private String login;
	
	private int idError;
	
	private String mensajeError;
	
	private String serialNumberGemalto;
	
	private String activationCodeGemalto;
	
	private String tokenAlias;

	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getSerialNumberGemalto() {
		return serialNumberGemalto;
	}
	public void setSerialNumberGemalto(String serialNumberGemalto) {
		this.serialNumberGemalto = serialNumberGemalto;
	}
	public String getActivationCodeGemalto() {
		return activationCodeGemalto;
	}
	public void setActivationCodeGemalto(String activationCodeGemalto) {
		this.activationCodeGemalto = activationCodeGemalto;
	}
	public String getTokenAlias() {
		return tokenAlias;
	}
	public void setTokenAlias(String tokenAlias) {
		this.tokenAlias = tokenAlias;
	}
}
