package com.addcel.telecom.usuarios.ws.servicios.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.addcel.telecom.usuarios.model.UsuarioDao;
import com.addcel.telecom.usuarios.model.vo.RespuestaVO;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.utils.ibatis.core.SessionApplicationContextBuilderImpl;
import com.addcel.telecom.usuarios.ws.servicios.vo.UsuarioRequest;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

public class PassWordCallbackBridge implements CallbackHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(PassWordCallbackBridge.class);
	
	private Gson gson = new Gson();

	@SuppressWarnings("unused")
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		logger.info(Constantes.PROCESO_VALIDACION_ACCESO_WS);
		for (int i = 0; i < callbacks.length; i++) { 
            WSPasswordCallback pwcb = (WSPasswordCallback)callbacks[i]; 
            UsuarioRequest usuario = new UsuarioRequest();
            RespuestaVO respuesta = null;
            printLoggerDebug(pwcb);
            respuesta = loginComercio(usuario, pwcb);
            if(respuesta.getIdError() == 0) {  
            	logger.info(Constantes.LOG_USUARIO_AUTENTICADO_EXITO+Constantes.LOG_PIPE+"["+pwcb.getIdentifier()+"]");
                return ;  
            } else {  
            	logger.info(Constantes.LOG_USUARIO_AUTENTICADO_ERROR+Constantes.LOG_PIPE+"["+pwcb.getIdentifier()+"]");
                throw new UnsupportedCallbackException(callbacks[i], Constantes.ERROR_WS_USUARIO_NO_VALIDO);  
            }  
        }  			
		logger.info(Constantes.PROCESO_FIN_VALIDACION_ACCESO_WS);
	}

	private RespuestaVO loginComercio(UsuarioRequest usuario, WSPasswordCallback pwcb) {
		UsuarioDao dao = null;
		RespuestaVO respuesta = null;
		usuario.setLogin(pwcb.getIdentifier());
        ///usuario.setPassword(AddcelCrypto.encryptMD5(pwcb.getPassword()));
        try {
			dao = (UsuarioDao) getBean("UsuarioDao");
			//dao.loginComercios(usuario);
            respuesta = gson.fromJson(usuario.getRespuesta(), RespuestaVO.class);
		} catch (Exception e) {
			e.printStackTrace();
			respuesta.setIdError(-1);
		}
        return respuesta;
	}

	private void printLoggerDebug(WSPasswordCallback pwcb) {
		if(logger.isDebugEnabled()){
        	logger.debug(Constantes.LOG_ASTERISCO);
    		logger.debug(Constantes.LOG_USER_LABEL + Constantes.LOG_PIPE + pwcb.getIdentifier());
    		logger.debug(Constantes.LOG_USER_PASSWORD_LABEL + Constantes.LOG_PIPE + "*******" );
    		logger.debug(Constantes.LOG_ASTERISCO);
        }
	}
	
	public Object getBean(String bean) throws Exception{
		ClassPathXmlApplicationContext ctxt = null;
		Object obj = null;
		try{
			ctxt = getSqlSessionInstance();
			obj =  ctxt.getBean(bean);
		}catch(Exception e){
			logger.error("Ocurrio un error al obtener el DAOBean.", e);
			throw new Exception(e);
		}
		return obj;
	}
	
	public ClassPathXmlApplicationContext getSqlSessionInstance(){
		return SessionApplicationContextBuilderImpl.getApplicationContexInstance();
	}
}