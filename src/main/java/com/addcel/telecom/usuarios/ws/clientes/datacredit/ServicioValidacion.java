/**
 * ServicioValidacion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.datacredit;

public interface ServicioValidacion extends javax.xml.rpc.Service {
    public java.lang.String getServicioValidacionAddress();

    public com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl getServicioValidacion() throws javax.xml.rpc.ServiceException;

    public com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl getServicioValidacion(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
