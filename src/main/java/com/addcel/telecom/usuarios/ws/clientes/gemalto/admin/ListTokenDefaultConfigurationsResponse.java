/**
 * ListTokenDefaultConfigurationsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class ListTokenDefaultConfigurationsResponse  implements java.io.Serializable {
//    private com.addcel.usuarios.ws.clientes.gemalto.admin.ListTokenDefaultConfigurationsResponseListTokenDefaultConfigurationsResult listTokenDefaultConfigurationsResult;

    public ListTokenDefaultConfigurationsResponse() {
    }

   /*  public ListTokenDefaultConfigurationsResponse(
          com.addcel.usuarios.ws.clientes.gemalto.admin.ListTokenDefaultConfigurationsResponseListTokenDefaultConfigurationsResult listTokenDefaultConfigurationsResult) {
           this.listTokenDefaultConfigurationsResult = listTokenDefaultConfigurationsResult;
    }*/


    /**
     * Gets the listTokenDefaultConfigurationsResult value for this ListTokenDefaultConfigurationsResponse.
     * 
     * @return listTokenDefaultConfigurationsResult
     */
    /*public com.addcel.usuarios.ws.clientes.gemalto.admin.ListTokenDefaultConfigurationsResponseListTokenDefaultConfigurationsResult getListTokenDefaultConfigurationsResult() {
        return listTokenDefaultConfigurationsResult;
    }*/


    /**
     * Sets the listTokenDefaultConfigurationsResult value for this ListTokenDefaultConfigurationsResponse.
     * 
     * @param listTokenDefaultConfigurationsResult
     */
    /*public void setListTokenDefaultConfigurationsResult(com.addcel.usuarios.ws.clientes.gemalto.admin.ListTokenDefaultConfigurationsResponseListTokenDefaultConfigurationsResult listTokenDefaultConfigurationsResult) {
        this.listTokenDefaultConfigurationsResult = listTokenDefaultConfigurationsResult;
    }*/
/*
    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListTokenDefaultConfigurationsResponse)) return false;
        ListTokenDefaultConfigurationsResponse other = (ListTokenDefaultConfigurationsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.listTokenDefaultConfigurationsResult==null && other.getListTokenDefaultConfigurationsResult()==null) || 
             (this.listTokenDefaultConfigurationsResult!=null &&
              this.listTokenDefaultConfigurationsResult.equals(other.getListTokenDefaultConfigurationsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getListTokenDefaultConfigurationsResult() != null) {
            _hashCode += getListTokenDefaultConfigurationsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListTokenDefaultConfigurationsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">ListTokenDefaultConfigurationsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listTokenDefaultConfigurationsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "ListTokenDefaultConfigurationsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">>ListTokenDefaultConfigurationsResponse>ListTokenDefaultConfigurationsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }*/

    /**
     * Return type metadata object
     */
    /*public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }*/

    /**
     * Get Custom Serializer
     */
    /*public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }*/

    /**
     * Get Custom Deserializer
     */
    /*public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }*/

}
