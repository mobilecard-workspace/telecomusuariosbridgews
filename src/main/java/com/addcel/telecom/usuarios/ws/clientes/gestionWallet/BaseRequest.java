package com.addcel.telecom.usuarios.ws.clientes.gestionWallet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseRequest {
 
	private String Idioma;

	public BaseRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public String getIdioma() {
		return Idioma;
	}

	public void setIdioma(String idioma) {
		Idioma = idioma;
	}
	
	
}
