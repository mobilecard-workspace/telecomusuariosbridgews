/**
 * MailSenderWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.mailSenderWS;

public interface MailSenderWS extends java.rmi.Remote {
    public java.lang.String enviaCorreo(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String envioSMS(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String enviaCorreoMandrill(java.lang.String json) throws java.rmi.RemoteException;
}
