/**
 * GetTokenDefaultConfigurationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class GetTokenDefaultConfigurationResponse  implements java.io.Serializable {
    //private com.addcel.usuarios.ws.clientes.gemalto.admin.GetTokenDefaultConfigurationResponseGetTokenDefaultConfigurationResult getTokenDefaultConfigurationResult;

    public GetTokenDefaultConfigurationResponse() {
    }

   /* public GetTokenDefaultConfigurationResponse(
           com.addcel.usuarios.ws.clientes.gemalto.admin.GetTokenDefaultConfigurationResponseGetTokenDefaultConfigurationResult getTokenDefaultConfigurationResult) {
           this.getTokenDefaultConfigurationResult = getTokenDefaultConfigurationResult;
    }*/


    /**
     * Gets the getTokenDefaultConfigurationResult value for this GetTokenDefaultConfigurationResponse.
     * 
     * @return getTokenDefaultConfigurationResult
     */
    /*public com.addcel.usuarios.ws.clientes.gemalto.admin.GetTokenDefaultConfigurationResponseGetTokenDefaultConfigurationResult getGetTokenDefaultConfigurationResult() {
        return getTokenDefaultConfigurationResult;
    }*/


    /**
     * Sets the getTokenDefaultConfigurationResult value for this GetTokenDefaultConfigurationResponse.
     * 
     * @param getTokenDefaultConfigurationResult
     */
    /*public void setGetTokenDefaultConfigurationResult(com.addcel.usuarios.ws.clientes.gemalto.admin.GetTokenDefaultConfigurationResponseGetTokenDefaultConfigurationResult getTokenDefaultConfigurationResult) {
        this.getTokenDefaultConfigurationResult = getTokenDefaultConfigurationResult;
    }*/

    /*private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTokenDefaultConfigurationResponse)) return false;
        GetTokenDefaultConfigurationResponse other = (GetTokenDefaultConfigurationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getTokenDefaultConfigurationResult==null && other.getGetTokenDefaultConfigurationResult()==null) || 
             (this.getTokenDefaultConfigurationResult!=null &&
              this.getTokenDefaultConfigurationResult.equals(other.getGetTokenDefaultConfigurationResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetTokenDefaultConfigurationResult() != null) {
            _hashCode += getGetTokenDefaultConfigurationResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTokenDefaultConfigurationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">GetTokenDefaultConfigurationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTokenDefaultConfigurationResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "GetTokenDefaultConfigurationResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">>GetTokenDefaultConfigurationResponse>GetTokenDefaultConfigurationResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }*/

    /**
     * Return type metadata object
     */
    /*public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }*/

    /**
     * Get Custom Serializer
     */
    /*public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }*/

    /**
     * Get Custom Deserializer
     */
    /*public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }*/

}
