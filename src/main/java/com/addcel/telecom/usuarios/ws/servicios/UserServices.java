package com.addcel.telecom.usuarios.ws.servicios;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.addcel.telecom.usuarios.model.vo.InsertRequest;
import com.addcel.telecom.usuarios.ws.servicios.vo.TUsuarios;


@Controller
public class UserServices {
	
	@RequestMapping(value = "ejemplo", method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public  ResponseEntity<String> example(){
		return  new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	@RequestMapping(value = "{idioma}/user/insert", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TUsuarios> UserInsert( @PathVariable String idioma, @RequestBody InsertRequest request) {
		TUsuarios usuario = new TUsuarios();
		usuario.setApellidoPrim("");
		usuario.setApellidoSeg("");
		usuario.setDireccion("");
		usuario.setDispositivo("");
		usuario.setEmail("");
		usuario.setFechaNacimiento("");
		usuario.setIdError(0);
		usuario.setIdeUsuario(12334l);
		usuario.setIdProveedor(1);
		usuario.setIdioma("es");
		usuario.setImei("");
		usuario.setMensajeError("");
		usuario.setNombres("");
		usuario.setPais(2);
		usuario.setUsrDireccion("");
		
		return  new ResponseEntity<TUsuarios>(usuario, HttpStatus.OK);
	}
	
}
