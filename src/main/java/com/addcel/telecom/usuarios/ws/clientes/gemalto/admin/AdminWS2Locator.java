/**
 * AdminWS2Locator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class AdminWS2Locator extends org.apache.axis.client.Service implements com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2 {

    public AdminWS2Locator() {
    }


    public AdminWS2Locator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AdminWS2Locator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for AdminWS2Soap
    private java.lang.String AdminWS2Soap_address = "http://54.244.231.177:8090/AdminWS/AdminWS2.asmx";

    public java.lang.String getAdminWS2SoapAddress() {
        return AdminWS2Soap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String AdminWS2SoapWSDDServiceName = "AdminWS2Soap";

    public java.lang.String getAdminWS2SoapWSDDServiceName() {
        return AdminWS2SoapWSDDServiceName;
    }

    public void setAdminWS2SoapWSDDServiceName(java.lang.String name) {
        AdminWS2SoapWSDDServiceName = name;
    }

    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap getAdminWS2Soap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(AdminWS2Soap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getAdminWS2Soap(endpoint);
    }

    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap getAdminWS2Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2SoapStub _stub = new com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2SoapStub(portAddress, this);
            _stub.setPortName(getAdminWS2SoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setAdminWS2SoapEndpointAddress(java.lang.String address) {
        AdminWS2Soap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2SoapStub _stub = new com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2SoapStub(new java.net.URL(AdminWS2Soap_address), this);
                _stub.setPortName(getAdminWS2SoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("AdminWS2Soap".equals(inputPortName)) {
            return getAdminWS2Soap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "AdminWS2");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "AdminWS2Soap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("AdminWS2Soap".equals(portName)) {
            setAdminWS2SoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
