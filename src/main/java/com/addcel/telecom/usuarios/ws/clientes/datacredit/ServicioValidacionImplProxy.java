package com.addcel.telecom.usuarios.ws.clientes.datacredit;

public class ServicioValidacionImplProxy implements com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl {
  private String _endpoint = null;
  private com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl servicioValidacionImpl = null;
  
  public ServicioValidacionImplProxy() {
    _initServicioValidacionImplProxy();
  }
  
  public ServicioValidacionImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicioValidacionImplProxy();
  }
  
  private void _initServicioValidacionImplProxy() {
    try {
      servicioValidacionImpl = (new com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionLocator()).getServicioValidacion();
      if (servicioValidacionImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicioValidacionImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicioValidacionImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicioValidacionImpl != null)
      ((javax.xml.rpc.Stub)servicioValidacionImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImpl getServicioValidacionImpl() {
    if (servicioValidacionImpl == null)
      _initServicioValidacionImplProxy();
    return servicioValidacionImpl;
  }
  
  public java.lang.String valCuenta(java.lang.String validadorCuentaXml) throws java.rmi.RemoteException{
    if (servicioValidacionImpl == null)
      _initServicioValidacionImplProxy();
    return servicioValidacionImpl.valCuenta(validadorCuentaXml);
  }
  
  
}