package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class AdminWS2SoapProxy implements com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap {
  private String _endpoint = null;
  private com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap adminWS2Soap = null;
  
  public AdminWS2SoapProxy() {
    _initAdminWS2SoapProxy();
  }
  
  public AdminWS2SoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initAdminWS2SoapProxy();
  }
  
  private void _initAdminWS2SoapProxy() {
    try {
      adminWS2Soap = (new com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Locator()).getAdminWS2Soap();
      if (adminWS2Soap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)adminWS2Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)adminWS2Soap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (adminWS2Soap != null)
      ((javax.xml.rpc.Stub)adminWS2Soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2Soap getAdminWS2Soap() {
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap;
  }
  
  public java.lang.String getVersion() throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getVersion();
  }
  
  public int associateHWToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String serialNumber) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.associateHWToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, serialNumber);
  }
  
  public int associateOfflineDevice(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tokenAlias, java.lang.String deviceIdBlock1, java.lang.String deviceIdBlock2, java.lang.String deviceIdBlock3) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.associateOfflineDevice(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenAlias, deviceIdBlock1, deviceIdBlock2, deviceIdBlock3);
  }
  
  public int activateToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tokenAlias, java.lang.String otp1, java.lang.String otp2) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.activateToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenAlias, otp1, otp2);
  }
  
  public int activateTokenWithChallengeResponse(java.lang.String fromDomainMnemonic, java.lang.String fromUserName, java.lang.String toDomainMnemonic, java.lang.String toUserName, java.lang.String tokenAlias, java.lang.String challenge, java.lang.String challengeControl, java.lang.String response) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.activateTokenWithChallengeResponse(fromDomainMnemonic, fromUserName, toDomainMnemonic, toUserName, tokenAlias, challenge, challengeControl, response);
  }
  
  public int activateOfflineToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tokenAlias, java.lang.String confirmationCodeBlock1, java.lang.String confirmationCodeBlock2, java.lang.String confirmationCodeBlock3) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.activateOfflineToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenAlias, confirmationCodeBlock1, confirmationCodeBlock2, confirmationCodeBlock3);
  }
  
  public void getTokenActivationCode(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tokenAlias, javax.xml.rpc.holders.IntHolder getTokenActivationCodeResult, javax.xml.rpc.holders.StringHolder serialNumber, javax.xml.rpc.holders.StringHolder activationCode) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    adminWS2Soap.getTokenActivationCode(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenAlias, getTokenActivationCodeResult, serialNumber, activationCode);
  }
  
  public void getTokenActivationChallenge(java.lang.String fromDomainMnemonic, java.lang.String fromUserName, java.lang.String toDomainMnemonic, java.lang.String toUserName, java.lang.String tokenAlias, javax.xml.rpc.holders.IntHolder getTokenActivationChallengeResult, javax.xml.rpc.holders.StringHolder challenge, javax.xml.rpc.holders.StringHolder challengeControl) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    adminWS2Soap.getTokenActivationChallenge(fromDomainMnemonic, fromUserName, toDomainMnemonic, toUserName, tokenAlias, getTokenActivationChallengeResult, challenge, challengeControl);
  }
  
  public int syncronizeToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId, int methodId, java.lang.String otp1, java.lang.String otp2) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.syncronizeToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId, methodId, otp1, otp2);
  }
  
  public int dissociateHWToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String serialNumber) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.dissociateHWToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, serialNumber);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetUserTokensResponseGetUserTokensResult getUserTokens(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getUserTokens(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetUserResponseGetUserResult getUser(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getUser(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetChannelFactorsResult getChannelConfig(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale, java.lang.String format) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getChannelConfig(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale, format);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.SetChannelFactorsResult setChannelConfig(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale, java.lang.String format, java.lang.String data) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.setChannelConfig(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale, format, data);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.CreateTokenResponseCreateTokenResult createToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, int tokenDefaultConfigurationId, java.lang.String tokenAlias, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.createToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenDefaultConfigurationId, tokenAlias, locale);
  }
  
  public void createUserToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String profileName, java.lang.String tokenAlias, javax.xml.rpc.holders.IntHolder createUserTokenResult, javax.xml.rpc.holders.StringHolder serialNumber, javax.xml.rpc.holders.StringHolder activationCode, javax.xml.rpc.holders.StringHolder createdTokenAlias) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    adminWS2Soap.createUserToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, profileName, tokenAlias, createUserTokenResult, serialNumber, activationCode, createdTokenAlias);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListTokenConfigResponseListTokenConfigResult listTokenConfig(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.listTokenConfig(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public void getTokenStatus(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId, javax.xml.rpc.holders.IntHolder getTokenStatusResult, javax.xml.rpc.holders.IntHolder tokenStatusId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    adminWS2Soap.getTokenStatus(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId, getTokenStatusResult, tokenStatusId);
  }
  
  public int cancelToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.cancelToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId);
  }
  
  public int blockToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.blockToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId);
  }
  
  public int unblockToken(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.unblockToken(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId);
  }
  
  public void getTokenMethodStatus(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId, int methodId, javax.xml.rpc.holders.IntHolder getTokenMethodStatusResult, javax.xml.rpc.holders.IntHolder tokenMethodStatusId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    adminWS2Soap.getTokenMethodStatus(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId, methodId, getTokenMethodStatusResult, tokenMethodStatusId);
  }
  
  public int cancelTokenMethod(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId, int methodId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.cancelTokenMethod(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId, methodId);
  }
  
  public int blockTokenMethod(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId, int methodId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.blockTokenMethod(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId, methodId);
  }
  
  public int unblockTokenMethod(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tokenId, int methodId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.unblockTokenMethod(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenId, methodId);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AddUserResponseAddUserResult addUser(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.addUser(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTokenPropertiesResponseGetTokenPropertiesResult getTokenProperties(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tokenAlias, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getTokenProperties(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenAlias, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetUserTANsResponseGetUserTANsResult getUserTANs(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale, boolean showCancelled) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getUserTANs(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale, showCancelled);
  }
  
  public int associateTAN(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tanAssociationId, java.lang.String tanSerial) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.associateTAN(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanAssociationId, tanSerial);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANStatusResponseGetTANStatusResult getTANStatus(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tanSerial, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getTANStatus(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSerial, locale);
  }
  
  public int activateTAN(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tanSerial) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.activateTAN(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSerial);
  }
  
  public int blockTAN(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tanSerial) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.blockTAN(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSerial);
  }
  
  public int unblockTAN(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tanSerial) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.unblockTAN(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSerial);
  }
  
  public int cancelTAN(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tanSerial) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.cancelTAN(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSerial);
  }
  
  public int sendVirtualTAN(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String tanSerial) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.sendVirtualTAN(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSerial);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListTANProfilesResponseListTANProfilesResult listTANProfiles(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.listTANProfiles(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public int createTAN(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, int tanProfileId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.createTAN(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanProfileId);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANPropertiesResponseGetTANPropertiesResult getTANProperties(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, int tanAssociationId, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getTANProperties(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanAssociationId, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANSetsResponseGetTANSetsResult getTANSets(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, boolean showCancelled, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getTANSets(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, showCancelled, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListPublishersResponseListPublishersResult listPublishers(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.listPublishers(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public int createTANSet(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, int tanProfileId, int quantity, int publisherId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.createTANSet(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanProfileId, quantity, publisherId);
  }
  
  public int exportTANSet(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tanSetId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.exportTANSet(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSetId);
  }
  
  public int activateTANSet(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tanSetId) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.activateTANSet(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSetId);
  }
  
  public int cancelTANSet(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tanSetId, java.lang.String notes) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.cancelTANSet(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSetId, notes);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANSetPropertiesResponseGetTANSetPropertiesResult getTANSetProperties(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, long tanSetId, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getTANSetProperties(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tanSetId, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListDomainsResponseListDomainsResult listDomains(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.listDomains(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListDomainsInUserRolesResponseListDomainsInUserRolesResult listDomainsInUserRoles(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.listDomainsInUserRoles(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListUsersResponseListUsersResult listUsers(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.listUsers(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }
  
  /*public com.addcel.usuarios.ws.clientes.gemalto.admin.GetTokenDefaultConfigurationResponseGetTokenDefaultConfigurationResult getTokenDefaultConfiguration(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, int tokenDefaultConfigurationId, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.getTokenDefaultConfiguration(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, tokenDefaultConfigurationId, locale);
  }
  
  public com.addcel.usuarios.ws.clientes.gemalto.admin.ListTokenDefaultConfigurationsResponseListTokenDefaultConfigurationsResult listTokenDefaultConfigurations(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.listTokenDefaultConfigurations(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, locale);
  }*/
  
  public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.SearchUsersResponseSearchUsersResult searchUsers(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, java.lang.String userMask, java.lang.String locale) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    return adminWS2Soap.searchUsers(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, userMask, locale);
  }
  
  public void getUserProfile(java.lang.String fromDomainMnemonic, java.lang.String fromUser, java.lang.String toDomainMnemonic, java.lang.String toUser, javax.xml.rpc.holders.IntHolder getUserProfileResult, javax.xml.rpc.holders.StringHolder profiles) throws java.rmi.RemoteException{
    if (adminWS2Soap == null)
      _initAdminWS2SoapProxy();
    adminWS2Soap.getUserProfile(fromDomainMnemonic, fromUser, toDomainMnemonic, toUser, getUserProfileResult, profiles);
  }
  
  
}