package com.addcel.telecom.usuarios.ws.servicios.vo;

public class UsuarioRequest {
	
	private String idUsuario = "";
	private String login;
	private String password;
	private String newPassword;
	private String telefono;
	private int operador;
	private String email;
	private String nombres;
	private String apellidoPrim;
	private String apellidoSeg;
	private String fechaNacimiento;
	private int genero;
	private String telefonoCasa;
	private String telefonoOficina;
	private int pais;
	private String direccion;
	
	private String tarjeta;
	private String vigencia;
	private String tarjetaCif;
	
	private String plataforma;
	private String dispositivo;
	private int idAplicacion;
	private int sms;
	private int terminos;
	
	private String respuesta;
	
	private boolean usuarioMigrado;
	
        private String idioma;
        
        public String getIdioma() {
        return idioma;
        }

        public void setIdioma(String idioma) {
            this.idioma = idioma;
        }
        
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public int getOperador() {
		return operador;
	}
	public void setOperador(int operador) {
		this.operador = operador;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPrim() {
		return apellidoPrim;
	}
	public void setApellidoPrim(String apellidoPrim) {
		this.apellidoPrim = apellidoPrim;
	}
	public String getApellidoSeg() {
		return apellidoSeg;
	}
	public void setApellidoSeg(String apellidoSeg) {
		this.apellidoSeg = apellidoSeg;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public int getGenero() {
		return genero;
	}
	public void setGenero(int genero) {
		this.genero = genero;
	}
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoOficina() {
		return telefonoOficina;
	}
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}
	public int getPais() {
		return pais;
	}
	public void setPais(int pais) {
		this.pais = pais;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	public String getDispositivo() {
		return dispositivo;
	}
	public void setDispositivo(String dispositivo) {
		this.dispositivo = dispositivo;
	}
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public int getSms() {
		return sms;
	}
	public void setSms(int sms) {
		this.sms = sms;
	}
	public int getTerminos() {
		return terminos;
	}
	public void setTerminos(int terminos) {
		this.terminos = terminos;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getTarjetaCif() {
		return tarjetaCif;
	}
	public void setTarjetaCif(String tarjetaCif) {
		this.tarjetaCif = tarjetaCif;
	}
	public boolean isUsuarioMigrado() {
		return usuarioMigrado;
	}
	public void setUsuarioMigrado(boolean usuarioMigrado) {
		this.usuarioMigrado = usuarioMigrado;
	}
}
