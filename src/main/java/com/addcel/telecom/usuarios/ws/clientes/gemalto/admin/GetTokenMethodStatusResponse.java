/**
 * GetTokenMethodStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class GetTokenMethodStatusResponse  implements java.io.Serializable {
    private int getTokenMethodStatusResult;

    private int tokenMethodStatusId;

    public GetTokenMethodStatusResponse() {
    }

    public GetTokenMethodStatusResponse(
           int getTokenMethodStatusResult,
           int tokenMethodStatusId) {
           this.getTokenMethodStatusResult = getTokenMethodStatusResult;
           this.tokenMethodStatusId = tokenMethodStatusId;
    }


    /**
     * Gets the getTokenMethodStatusResult value for this GetTokenMethodStatusResponse.
     * 
     * @return getTokenMethodStatusResult
     */
    public int getGetTokenMethodStatusResult() {
        return getTokenMethodStatusResult;
    }


    /**
     * Sets the getTokenMethodStatusResult value for this GetTokenMethodStatusResponse.
     * 
     * @param getTokenMethodStatusResult
     */
    public void setGetTokenMethodStatusResult(int getTokenMethodStatusResult) {
        this.getTokenMethodStatusResult = getTokenMethodStatusResult;
    }


    /**
     * Gets the tokenMethodStatusId value for this GetTokenMethodStatusResponse.
     * 
     * @return tokenMethodStatusId
     */
    public int getTokenMethodStatusId() {
        return tokenMethodStatusId;
    }


    /**
     * Sets the tokenMethodStatusId value for this GetTokenMethodStatusResponse.
     * 
     * @param tokenMethodStatusId
     */
    public void setTokenMethodStatusId(int tokenMethodStatusId) {
        this.tokenMethodStatusId = tokenMethodStatusId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTokenMethodStatusResponse)) return false;
        GetTokenMethodStatusResponse other = (GetTokenMethodStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.getTokenMethodStatusResult == other.getGetTokenMethodStatusResult() &&
            this.tokenMethodStatusId == other.getTokenMethodStatusId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGetTokenMethodStatusResult();
        _hashCode += getTokenMethodStatusId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTokenMethodStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">GetTokenMethodStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTokenMethodStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "GetTokenMethodStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tokenMethodStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "tokenMethodStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
