/**
 * GetTANStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class GetTANStatusResponse  implements java.io.Serializable {
    private com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANStatusResponseGetTANStatusResult getTANStatusResult;

    public GetTANStatusResponse() {
    }

    public GetTANStatusResponse(
           com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANStatusResponseGetTANStatusResult getTANStatusResult) {
           this.getTANStatusResult = getTANStatusResult;
    }


    /**
     * Gets the getTANStatusResult value for this GetTANStatusResponse.
     * 
     * @return getTANStatusResult
     */
    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANStatusResponseGetTANStatusResult getGetTANStatusResult() {
        return getTANStatusResult;
    }


    /**
     * Sets the getTANStatusResult value for this GetTANStatusResponse.
     * 
     * @param getTANStatusResult
     */
    public void setGetTANStatusResult(com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.GetTANStatusResponseGetTANStatusResult getTANStatusResult) {
        this.getTANStatusResult = getTANStatusResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTANStatusResponse)) return false;
        GetTANStatusResponse other = (GetTANStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getTANStatusResult==null && other.getGetTANStatusResult()==null) || 
             (this.getTANStatusResult!=null &&
              this.getTANStatusResult.equals(other.getGetTANStatusResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetTANStatusResult() != null) {
            _hashCode += getGetTANStatusResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetTANStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">GetTANStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getTANStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "GetTANStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">>GetTANStatusResponse>GetTANStatusResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
