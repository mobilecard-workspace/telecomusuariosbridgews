/**
 * MailSenderWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.mailSenderWS;

public class MailSenderWSServiceLocator extends org.apache.axis.client.Service implements com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWSService {

    public MailSenderWSServiceLocator() {
    }


    public MailSenderWSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MailSenderWSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MailSenderWS
    private java.lang.String MailSenderWS_address = "http://localhost:8080/AddcelColombiaMailSender/services/MailSenderWS";

    public java.lang.String getMailSenderWSAddress() {
        return MailSenderWS_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MailSenderWSWSDDServiceName = "MailSenderWS";

    public java.lang.String getMailSenderWSWSDDServiceName() {
        return MailSenderWSWSDDServiceName;
    }

    public void setMailSenderWSWSDDServiceName(java.lang.String name) {
        MailSenderWSWSDDServiceName = name;
    }

    public com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWS getMailSenderWS() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MailSenderWS_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMailSenderWS(endpoint);
    }

    public com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWS getMailSenderWS(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWSSoapBindingStub _stub = new com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWSSoapBindingStub(portAddress, this);
            _stub.setPortName(getMailSenderWSWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMailSenderWSEndpointAddress(java.lang.String address) {
        MailSenderWS_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWS.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWSSoapBindingStub _stub = new com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWSSoapBindingStub(new java.net.URL(MailSenderWS_address), this);
                _stub.setPortName(getMailSenderWSWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MailSenderWS".equals(inputPortName)) {
            return getMailSenderWS();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servicios.ws.colombia.addcel.com", "MailSenderWSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servicios.ws.colombia.addcel.com", "MailSenderWS"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MailSenderWS".equals(portName)) {
            setMailSenderWSEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
