/**
 * ListTANProfilesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usuarios.ws.clientes.gemalto.admin;

public class ListTANProfilesResponse  implements java.io.Serializable {
    private com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListTANProfilesResponseListTANProfilesResult listTANProfilesResult;

    public ListTANProfilesResponse() {
    }

    public ListTANProfilesResponse(
           com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListTANProfilesResponseListTANProfilesResult listTANProfilesResult) {
           this.listTANProfilesResult = listTANProfilesResult;
    }


    /**
     * Gets the listTANProfilesResult value for this ListTANProfilesResponse.
     * 
     * @return listTANProfilesResult
     */
    public com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListTANProfilesResponseListTANProfilesResult getListTANProfilesResult() {
        return listTANProfilesResult;
    }


    /**
     * Sets the listTANProfilesResult value for this ListTANProfilesResponse.
     * 
     * @param listTANProfilesResult
     */
    public void setListTANProfilesResult(com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.ListTANProfilesResponseListTANProfilesResult listTANProfilesResult) {
        this.listTANProfilesResult = listTANProfilesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListTANProfilesResponse)) return false;
        ListTANProfilesResponse other = (ListTANProfilesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.listTANProfilesResult==null && other.getListTANProfilesResult()==null) || 
             (this.listTANProfilesResult!=null &&
              this.listTANProfilesResult.equals(other.getListTANProfilesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getListTANProfilesResult() != null) {
            _hashCode += getListTANProfilesResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListTANProfilesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">ListTANProfilesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listTANProfilesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", "ListTANProfilesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.otpmt.net/AuthenticationManagement/1.0", ">>ListTANProfilesResponse>ListTANProfilesResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
