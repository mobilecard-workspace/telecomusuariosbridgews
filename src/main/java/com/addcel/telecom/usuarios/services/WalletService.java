package com.addcel.telecom.usuarios.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import com.addcel.telecom.usuarios.utils.ibatis.service.AbstractService;
import com.addcel.telecom.usuarios.ws.clientes.gestionWallet.TarjetaRequest;
import com.addcel.telecom.usuarios.ws.clientes.gestionWallet.TarjetaRequestTC;
import com.addcel.telecom.usuarios.ws.clientes.gestionWallet.TarjetaResponse;

@Service
public class WalletService extends AbstractService {
	
	private static final String urlAddCard="http://localhost:8081/TelecomGestionWallet/telecom/add";
	private static final String urlUpdateCard="http://localhost:8081/TelecomGestionWallet/telecom/update";
	private static final String urlAddCardTelecom="http://localhost:8081/TelecomGestionWallet/telecom/addTelecom";
	private static final String urlEnrollTelecom="http://localhost:8081/TelecomGestionWallet/telecom/enrollCustomer";																							  
	
//	private static final String urlGetCard="http://localhost:8081/TelecomGestionWallet/telecom/getTarjetas";
//	private static final String urlAddCard="http://localhost:8080/TelecomGestionWallet/telecom/add";	
	
	private RestTemplate restTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WalletService.class);
	
	
	public boolean addCard(String cod, boolean determinada, String idioma, String nombre, String pan, String vigencia, long id_usuario, String cpAmex, String domAmex, boolean mobilecard)
	{
		
		try{
			restTemplate = (RestTemplate)getBean("restTemplate");
		//	restTemplate.setMessageConverters(getMessageConverters());
			LOGGER.debug("Iniciando proceso de guardado de tarjeta: " + id_usuario);
			TarjetaRequest Trequest = new TarjetaRequest();
			
			Trequest.setCodigo(cod);
			Trequest.setDeterminada(determinada);
			Trequest.setIdioma(idioma);
			Trequest.setIdTarjeta(0);
			Trequest.setIdUsuario(id_usuario);
			Trequest.setNombre(nombre);
			Trequest.setPan(pan);
			Trequest.setTipo(1);
			Trequest.setVigencia(vigencia);
			Trequest.setCpAmex(cpAmex);
			Trequest.setDomAmex(domAmex);
			Trequest.setMobilecard(mobilecard);
			
			HttpHeaders headers = new HttpHeaders();
		//	headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<TarjetaRequest> request = new HttpEntity<TarjetaRequest>(Trequest,headers);
			LOGGER.debug("Enviando Peticion");
			TarjetaResponse responseObject  =  (TarjetaResponse)  restTemplate.postForObject( urlAddCard, request, TarjetaResponse.class);
			
			LOGGER.debug("Finalizando guardado de tarjeta");
			
			if(responseObject.getIdError()==0)
				return true;
			else
				return false;
		}catch(Exception ex){
			//LOGGER.error("Error al agregar tarjeta: " + ex.getMessage());
			LOGGER.error("Error", ex);
			ex.printStackTrace();
			return false;
		}
		
	}
	
	
	
	public boolean addCardTelecom( long id_usuario,String cpAmex, String domAmex,String ciudad,String fechaNac,String idioma,String ssn,String govtIdIssueState,String govtIdIssueDate,String govtIdExpirationDate,String govtId,String estado)
	{
		
		try{
			restTemplate = (RestTemplate)getBean("restTemplate");
		//	restTemplate.setMessageConverters(getMessageConverters());
			LOGGER.debug("Iniciando proceso de guardado de tarjeta: " + id_usuario);
//			TarjetaRequest Trequest = new TarjetaRequest();
			TarjetaRequestTC Trequest = new TarjetaRequestTC();
			
			Trequest.setSsn(ssn);
			Trequest.setIdUsuario(id_usuario);			
			Trequest.setIdioma(idioma);
			Trequest.setGovtIdIssueState(govtIdIssueState);
			Trequest.setGovtIdIssueDate(govtIdIssueDate);
			Trequest.setGovtIdExpirationDate(govtIdExpirationDate);
			Trequest.setGovtId(govtId);
			Trequest.setCiudad(ciudad);
			Trequest.setEstado(estado);
			Trequest.setFechaNac(fechaNac);
			Trequest.setDireccion(domAmex);
			Trequest.setCp(cpAmex);
			
			
			
			
			
			
//			Trequest.setCodigo(cod);
//			Trequest.setDeterminada(determinada)
//			Trequest.setIdioma(idioma);
//			Trequest.setIdTarjeta(id_tarjeta);
//			Trequest.setIdUsuario(id_usuario);
//			Trequest.setNombre(nombre);
//			Trequest.setPan(pan);
//			Trequest.setTipo(1);
//			Trequest.setVigencia(vigencia);
//			Trequest.setCpAmex(cpAmex);
//			Trequest.setDomAmex(domAmex);
//			Trequest.setMobilecard(mobilecard);
			
			HttpHeaders headers = new HttpHeaders();
		//	headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<TarjetaRequestTC> request = new HttpEntity<TarjetaRequestTC>(Trequest,headers);
			LOGGER.debug("Enviando Peticion");
			TarjetaResponse responseObject  =  (TarjetaResponse)  restTemplate.postForObject(urlAddCardTelecom, request, TarjetaResponse.class);
			
			LOGGER.debug("Finalizando guardado de tarjeta");
			
			if(responseObject.getIdError()==0)
				return true;
			else
				return false;
		}catch(Exception ex){
			//LOGGER.error("Error al agregar tarjeta: " + ex.getMessage());
			LOGGER.error("Error", ex);
			ex.printStackTrace();
			return false;
		}
		
	}
	
	
	public boolean updateCard(String cod, boolean determinada, String idioma, String nombre, String pan, String vigencia, long id_usuario, String cpAmex, String domAmex, boolean mobilecard)
	{
		
		try{
			restTemplate = (RestTemplate)getBean("restTemplate");
		//	restTemplate.setMessageConverters(getMessageConverters());
			LOGGER.debug("Iniciando proceso de update de tarjeta: " + id_usuario);
			TarjetaRequest Trequest = new TarjetaRequest();
			
			Trequest.setCodigo(cod);
			Trequest.setDeterminada(determinada);
			Trequest.setIdioma(idioma);
			/*Esta quemado*/
			Trequest.setIdTarjeta(392);
			/*Esta quemado*/
			Trequest.setIdUsuario(id_usuario);
			Trequest.setNombre(nombre);
			Trequest.setPan(pan);
			Trequest.setTipo(1);
			Trequest.setVigencia(vigencia);
			Trequest.setCpAmex(cpAmex);
			Trequest.setDomAmex(domAmex);
			Trequest.setMobilecard(mobilecard);
			
			HttpHeaders headers = new HttpHeaders();
		//	headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<TarjetaRequest> request = new HttpEntity<TarjetaRequest>(Trequest,headers);
			LOGGER.debug("Enviando Peticion");
			TarjetaResponse responseObject  =  (TarjetaResponse)  restTemplate.postForObject( urlUpdateCard, request, TarjetaResponse.class);
			
			LOGGER.debug("Finalizando guardado de tarjeta");
			
			if(responseObject.getIdError()==0)
				return true;
			else
				return false;
		}catch(Exception ex){
			//LOGGER.error("Error al agregar tarjeta: " + ex.getMessage());
			LOGGER.error("Error", ex);
			ex.printStackTrace();
			return false;
		}
		
	}
	
	private List<HttpMessageConverter<?>> getMessageConverters() {
	    List<HttpMessageConverter<?>> converters = 
	      new ArrayList<HttpMessageConverter<?>>();
	    converters.add(new MappingJacksonHttpMessageConverter());
	    return converters;
	}
	
	
	
//	public boolean getCard(String idioma, long id_usuario)
//	{
//		
//		try{
//			restTemplate = (RestTemplate)getBean("restTemplate");
//		//	restTemplate.setMessageConverters(getMessageConverters());
//			LOGGER.debug("Iniciando proceso de obtener de tarjeta: " + id_usuario);
//			TarjetaRequest Trequest = new TarjetaRequest();		
//			Trequest.setIdioma(idioma);		
//			Trequest.setIdUsuario(id_usuario);			
//			HttpHeaders headers = new HttpHeaders();
//			//	headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
////			HttpEntity<TarjetaRequest> request = new HttpEntity<TarjetaRequest>(Trequest,headers);
//			LOGGER.debug("Enviando Peticion");
//			MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
//			map.add("idUsuario", id_usuario);
//			map.add("idioma", idioma);	
//			
//
//			TarjetaResponse responseObject  =  (TarjetaResponse)  restTemplate.postForObject( urlGetCard, map, TarjetaResponse.class);
//			LOGGER.debug("Finalizando obtener tarjeta");			
//			if(responseObject.getIdError()==0)
//				return true;
//			else
//				return false;
//		}catch(Exception ex){
//			//LOGGER.error("Error al agregar tarjeta: " + ex.getMessage());
//			LOGGER.error("Error", ex);
//			ex.printStackTrace();
//			return false;
//		}
//		
//	}	
}
