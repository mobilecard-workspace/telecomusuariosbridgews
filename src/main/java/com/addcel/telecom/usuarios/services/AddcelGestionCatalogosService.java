package com.addcel.telecom.usuarios.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.telecom.usuarios.model.CatalogoDao;
import com.addcel.telecom.usuarios.model.vo.CatalogoVO;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.utils.ibatis.service.AbstractService;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

public class AddcelGestionCatalogosService extends AbstractService{

	private static final Logger LOGGER = LoggerFactory.getLogger(AddcelGestionCatalogosService.class);
	
	private static final SimpleDateFormat ENCRIPT_DATE_FORMAT = new SimpleDateFormat(Constantes.FORMATO_ENCRIPT);
	
	private Gson gson = new Gson();
	
	public String consultaCatalogos(String json) {
		CatalogoVO catalogo = null;
		CatalogoDao dao = null;
		try {
//			String json_encriptado = AddcelCrypto.encryptSensitive(ENCRIPT_DATE_FORMAT.format(new Date()), json);
//			LOGGER.info("json_encriptado: "+json_encriptado);
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info("json_desencriptado: "+json);
			catalogo = gson.fromJson(json, CatalogoVO.class);
			dao = (CatalogoDao) getBean("CatalogoDao");
			LOGGER.info(Constantes.LOG_SERVICE_CATALOGO+Constantes.LOG_PIPE+Constantes.CATALOGO_SOLICITADO+catalogo.getIdCatalogo());
			if(Constantes.ID_CATALOGO_TERMINOS.equals(catalogo.getIdCatalogo())){
				json = dao.consultaTerminos(catalogo.getIdAplicacion());
			} else if(Constantes.ID_CATALOGO_PAIS.equals(catalogo.getIdCatalogo())){
				json = dao.consultaPais();
			} else if(Constantes.ID_CATALOGO_DOCUMENTOS.equals(catalogo.getIdCatalogo())){
				json = dao.consultaDocumentos(catalogo.getIdPais());
			} else if(Constantes.ID_CATALOGO_GENEROS.equals(catalogo.getIdCatalogo())){
				json = dao.consultaGeneros();
			} else if(Constantes.ID_CATALOGO_OPERADOR.equals(catalogo.getIdCatalogo())){
				json = dao.consultaOperador();
			} else if(Constantes.ID_CATALOGO_CIUDADES.equals(catalogo.getIdCatalogo())){
				json = dao.consultaCiudades();
			}  else {
				json = Constantes.ERROR_JSON_ID_CATALOGO_NO_VALIDO;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			json = Constantes.ERROR_JSON_CONSULTA_CATALOGOS;
		}finally{
			LOGGER.info("json REPUESTA: "+json);
			json = AddcelCrypto.encryptSensitive(ENCRIPT_DATE_FORMAT.format(new Date()), json);
		}
		return json;
	}
	
}