package com.addcel.telecom.usuarios.services;
import java.io.StringReader;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.rpc.holders.BooleanHolder;
import javax.xml.rpc.holders.IntHolder;
import javax.xml.rpc.holders.StringHolder;
import javax.mail.MessagingException;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import com.addcel.telecom.usuarios.model.UsuarioDao;
import com.addcel.telecom.usuarios.model.vo.CorreoVO;
import com.addcel.telecom.usuarios.model.vo.ExperianUserVO;
import com.addcel.telecom.usuarios.model.vo.GemaltoDataVO;
import com.addcel.telecom.usuarios.model.vo.MailSender;
import com.addcel.telecom.usuarios.model.vo.MovimientosTarjetaVO;
import com.addcel.telecom.usuarios.model.vo.SmsVO;
import com.addcel.telecom.usuarios.model.vo.TarjetaVO;
import com.addcel.telecom.usuarios.utils.AddCelGenericMail;
import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.utils.UtilsService;
import com.addcel.telecom.usuarios.utils.ibatis.service.AbstractService;
import com.addcel.telecom.usuarios.ws.clientes.datacredit.ServicioValidacionImplProxy;
import com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AddUserResponseAddUserResult;
import com.addcel.telecom.usuarios.ws.clientes.gemalto.admin.AdminWS2SoapProxy;
import com.addcel.telecom.usuarios.ws.clientes.gemalto.userRepository.UserRepositoryWSSoapProxy;
import com.addcel.telecom.usuarios.ws.clientes.mailSenderWS.MailSenderWSProxy;
import com.addcel.telecom.usuarios.ws.servicios.vo.AbstractVO;
import com.addcel.telecom.usuarios.ws.servicios.vo.IngoEnroleVO;
import com.addcel.telecom.usuarios.ws.servicios.vo.ResponseEnroleIngo;
import com.addcel.telecom.usuarios.ws.servicios.vo.TUsuarios;
import com.addcel.telecom.usuarios.ws.servicios.vo.UsuarioRequest;
import com.addcel.telecom.usuarios.ws.servicios.vo.UsuarioResponse;
import com.addcel.telecom.usuarios.ws.servicios.vo.UsuarioVO;
import com.addcel.telecom.usuarios.ws.servicios.vo.parametroVO;
import com.addcel.utils.AddcelCrypto;
// import com.addcel.utils.AddcelCryptoRSA;
import com.google.gson.Gson;
import com.google.gson.stream.MalformedJsonException;
import com.sun.mail.smtp.SMTPTransport;

import crypto.Crypto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;

@Service
public class AddcelGestionUsuariosService extends AbstractService {

  // private static final String URL_HEADER_ADDCEL =
  // "\\WEB-INF\\classes\\images\\Mobilecard_header.png";

  private final Properties properties = new Properties();


  private String password;

  private Session session;
  //@Autowired
 // private JavaMailSenderImpl mailSender;
  @Autowired
  private WalletService wallet;

  private static final Logger LOGGER = LoggerFactory.getLogger(AddcelGestionUsuariosService.class);

  private Gson gson = new Gson();

  private static final String urlString = "http://localhost:8081/TelecomIngo/EnrollCustomer";// url para
                                                                                        // enviar
                                                                                        // datos a
                                                                                        // ingo

  private static final String FORMATO_FECHA_ENTRADA = "yyyy/MM/dd";

  private static final String FORMATO_FECHA_ENTRADA_BASE = "yyyy-MM-dd";

  private static final String FORMATO_FECHA_SALIDA = "yyyy/MM/dd";

  private static final SimpleDateFormat FORMATO_CIFRADO = new SimpleDateFormat(
      Constantes.FORMATO_ENCRIPT);

  private static final String USUARIO_DAO = "UsuarioDao";

  private static final Object CODIGO_RESPUESTA_DATACREDIT = "codigoRespuesta";

  private static Properties PROPS = null;
  private static String HOST = null;
  private static int PORT = 0;
  private static String USERNAME = null;
  private static String PASSWORD = null;
  private static String FROM = null;

  public String guardarUsuario(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    String email = null;
    String password = null;
    String usuario = null;
    String codigo;
    String idi = null;
    String keyaux = "1234567890ABCDEF0123456789ABCDEF";
    String cpAmex = null;
    String domAmex = null;
    boolean DEBUG = false;
    IngoEnroleVO objingo = new IngoEnroleVO();// objeto para enrolamiento de ingo
    int status = 0;
    try {
      // json = AddcelCrypto.decryptSensitive(json);
      LOGGER.info("json : " + json);
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      email = usuarioVO.geteMail();
      usuario = usuarioVO.getUsrLogin();
      idi = usuarioVO.getIdioma();
      codigo = usuarioVO.getUsrTdcCodigo();
      cpAmex = usuarioVO.getUsrCp();
      domAmex = usuarioVO.getUsrDomAmex();
     
      DEBUG =  usuarioVO.getDebug();
      /*
       * if(validaDataCredit(usuarioVO.getI)){ /*if(!realizaValidacionDataCredit(usuarioVO)){ json =
       * Constantes.JSON_ERROR_VALIDA_DATACREDIT; json =
       * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); return json; } }
       */
      dao = (UsuarioDao) getBean(USUARIO_DAO); 
      password = UtilsService.generatePassword(8);
      
//      password = "2EJWoqj8";

      LOGGER.info(Constantes.LOG_SERVICE_GUARDA_USUARIO + Constantes.LOG_FORM_USUARIO
          + usuarioVO.getUsrLogin());      
      String key = generaSemillaAux(password);      
      /*
       * String reppass = Crypto.aesEncrypt(key, password); String repp =
       * reppass.replace("\u003d\u003d", ""); LOGGER.info("passmodificado : " + repp);
       */
      usuarioVO.setUsrPwd(Crypto.aesEncrypt(key, password));        
      String deshardtar = AddcelCrypto.decryptHard(usuarioVO.getUsrTdcNumero());
      String reptar = UtilsService.setSMS(deshardtar);
      usuarioVO.setUsrTdcNumero(reptar);      
      String deshardvigencia = AddcelCrypto.decryptHard(usuarioVO.getUsrTdcVigencia());
      String reptarvigencia = UtilsService.setSMS(deshardvigencia);
      usuarioVO.setUsrTdcVigencia(deshardvigencia);
      json = gson.toJson(usuarioVO);
      LOGGER.info("datos enviados para guardar : " + json);
      
      
      // usuarioVO.setUsr_tdc_vigencia(AddcelCryptoRSA.encryptPublic(usuarioVO.getUsr_tdc_vigencia()));
      /*
       * usuarioVO.setUsr_fecha_nac(UtilsService.cambiarFormatoFecha( usuarioVO.getUsr_fecha_nac(),
       * FORMATO_FECHA_ENTRADA, FORMATO_FECHA_SALIDA));
       */
      /* se llenan los datos para el enrolamiento de ingo */
      // /objingo = ingoEnrole(usuarioVO);
      usuarioVO.setIdUsrStatus(99);
      status = dao.guardarUsuario(usuarioVO);
      // json = getRespuesta(usuarioVO, status);
      usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
      

      LOGGER.info(Constantes.LOG_SERVICE_GUARDA_USUARIO + Constantes.LOG_FORM_ID_USUARIO + usuario
          + Constantes.LOG_FORM_USUARIO + usuario);

      json = gson.toJson(usuarioVO);

      // usuarioVO = gson.fromJson(json, TUsuarios.class);

      if (json.contains("Usuario insertado con exito")
          || json.contains("User inserted successfully")) {
        // sendSms(usuarioVO, password, MODULE_BIENVENIDO);
        // /String jsoningo = gson.toJson(objingo);
        // String responsejsoningo = sendIngo(jsoningo);
        String responsejsoningo =
            "{\"errorCode\":0,\"errorMessage\":\"\",\"customerId\":\"6ff553f0-015a-416a-98cd-6232bd979b62\"}";

        ResponseEnroleIngo rei = new ResponseEnroleIngo();
        rei = gson.fromJson(responsejsoningo, ResponseEnroleIngo.class);

        rei.setUsrLogin(usuario);
        rei.setIdioma(idi);

        LOGGER.info("jsoningo : " + responsejsoningo);
        LOGGER.info("errorCode : " + rei.getErrorCode());
        LOGGER.info("errorMessage : " + rei.getErrorMessage());
        LOGGER.info("customerId : " + rei.getCustomerId());
        LOGGER.info("usrLogin : " + rei.getUsrLogin());
        LOGGER.info("idioma : " + rei.getIdioma());
        //dao.actualizarUsuarioIdIngo(rei);
        
        
        //Guardar Tarjeta de usuario 
        
        TUsuarios usuariologin = null;
        String login = loginUsuario("{\"idioma\":\"" +idi + "\",\"usrLogin\":\"" + usuario + "\",\"usrPwd\":\"" +AddcelCrypto.encryptHard(password) + "\"}");
        LOGGER.debug("respuesta login: " + login);
        usuariologin = gson.fromJson(login, TUsuarios.class);
        
        if(deshardtar != null && !deshardtar.isEmpty())
        {
	        
	        LOGGER.debug("object: " + idi + " " + usuariologin.getUsrNombre()  + " " + deshardtar + " " + deshardvigencia + " " + usuariologin.getIdeUsuario() + " " + codigo);
	        if(wallet == null )
	        {
	        	wallet = new WalletService();
	        	LOGGER.debug("iniciando wallet");
	        }
	        dao = null;
	        wallet.addCard(codigo, true, idi, usuariologin.getUsrNombre(), AddcelCrypto.encryptHard(deshardtar), AddcelCrypto.encryptHard(deshardvigencia), usuariologin.getIdeUsuario(),cpAmex,domAmex,false);
        //////////
        }
        else ///No se detecto Tarjeta enviar codigo error 90 para obtener tarjeta Mobilecard
        {
        	usuarioVO.setIdError(90);
        	usuarioVO.setIdUsuario(usuariologin.getIdeUsuario());
        	usuarioVO.setIdPais(3); //TODO usuariologin.getIdPais() 
        	if(idi.equals("es"))
        		usuarioVO.setMensajeError("Desea obtener tarjeta Mobilecard");
        	else
        		usuarioVO.setMensajeError("Want to get Mobilecard card");
        }
        
        
        
        if (idi.equals("es")) {
          if (UtilsService.patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*")) {            
            SendMailMicrosoft(email, "@MENSAJE_REGISTRO_TELECOM_ES", "@ASUNTO_REGISTRO_TELECOM_ES", usuario, password);
          } else {            
            SendMailNormal(email, "@MENSAJE_REGISTRO_TELECOM_ES", "@ASUNTO_REGISTRO_TELECOM_ES", usuario,
                    password);
          }
        } else {
          if (UtilsService.patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*")) {            
            SendMailMicrosoft(email, "@MENSAJE_REGISTRO_TELECOM_EN", "@ASUNTO_REGISTRO_TELECOM_EN", usuario,
                password);
          } else {            
            SendMailNormal(email, "@MENSAJE_REGISTRO_TELECOM_EN", "@ASUNTO_REGISTRO_TELECOM_EN", usuario,
                    password);
          }
        }

      }

      //Para realizar pruebas en QA
      if(DEBUG)
    	  usuarioVO.setDebugPass(password);
      
      json = gson.toJson(usuarioVO);

    } catch (Exception e) {
      e.printStackTrace();
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info("json : " + json);
    return json;
  }

  public static String generaSemilla(String pass) {
    String sem = "";
    for (int i = 0; i <= 3; i++) {
      sem += pass;
    }
    return sem;
  }

  public static String generaSemillaAux(String pass) {
    int len = pass.length();
    String key = "";

    int carry;
    for (carry = 0; carry < 32 / len; ++carry) {
      key = key + pass;
    }

    for (carry = 0; key.length() < 32; ++carry) {
      key = key + pass.charAt(carry);
    }

    return key;
  }

  public static String sendIngo(String data) {
    String line = null;
    StringBuilder sb = new StringBuilder();
    try {
      LOGGER.info("Iniciando proceso de envio datos ingo ");
      URL url = new URL(urlString);
      LOGGER.info("Conectando con " + urlString);
      HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


      Authenticator au = new Authenticator() {
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication("guest", "guest".toCharArray());
        }
      };
      Authenticator.setDefault(au);
      urlConnection.setDoOutput(true);
      urlConnection.setRequestProperty("Content-Type", "application/json");
      urlConnection.setRequestProperty("Accept", "application/json");
      urlConnection.setRequestMethod("POST");

      OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
      writter.write(data);
      writter.flush();

      LOGGER.info("Datos enviados, esperando respuesta");

      BufferedReader reader =
          new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

      while ((line = reader.readLine()) != null) {
        sb.append(line);
      }

      LOGGER.info("Respuesta del servidor(envio datos enrolamiento ingo) " + sb.toString());
    } catch (Exception ex) {
      LOGGER.error("Error en: sendIngo, al enviar los datos ", ex);
    }
    return sb.toString();
  }

  public IngoEnroleVO ingoEnrole(TUsuarios tusu) {
    IngoEnroleVO objingo = new IngoEnroleVO();
    objingo.setEmail(tusu.geteMail());
    objingo.setSsn(tusu.getUsrNss());
    objingo.setFirstName(tusu.getUsrNombre());
    objingo.setLastName(tusu.getUsrApellido());
    objingo.setMiddleInitial("");
    objingo.setTitle("");
    objingo.setSuffix("");
    objingo.setMobileNumber(tusu.getTelefonoOriginal());
    objingo.setHomeNumber(tusu.getTelefonoCasa());
    objingo.setAddressLine1(tusu.getUsrDireccion());
    objingo.setAddressLine2("");
    objingo.setCity(tusu.getUsrCiudad());
    objingo.setState("TN");
    objingo.setZip(tusu.getUsrCp());
    objingo.setDateOfBirth(tusu.getUsrFechaNac());
    objingo.setCountryOfOrigin(tusu.getUsrCiudad());
    objingo.setGender(tusu.getUsrSexo());
    objingo.setAllowTexts(true);
    objingo.setCustomerId("");
    objingo.setCardNumber(tusu.getUsrTdcNumero());
    objingo.setExpirationMonthYear(tusu.getUsrTdcVigencia());
    objingo.setCardNickname("Mi primer tarjeta");
    objingo.setNameOnCard(tusu.getUsrNombre());
    return objingo;
  }
  
  public String loginUsuario(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    parametroVO p1 = new parametroVO();
    try {
      LOGGER.info("loginUsuario");
      String passwordb = "";
      String keyaux = "1234567890ABCDEF0123456789ABCDEF";
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_LOGIN + usuarioVO.getUsrLogin());
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      p1.setParam(usuarioVO.getUsrLogin());
      /* Descomentar cuando se libere lo de colombia */
      // passwordb = dao.consultaPassword(p1);
      usuarioVO.setLogin(usuarioVO.getUsrLogin());
      /*
       * if(passwordb.length() <= 40){ String passdecrypt =
       * AddcelCryptoRSA.decryptPrivate(usuarioVO.getUsrPwd()); String passmx =
       * generaSemillaAux(passdecrypt); LOGGER.info("passmx : " + Crypto.aesEncrypt(passmx,
       * passdecrypt)); usuarioVO.setPassword(Crypto.aesEncrypt(passmx, passdecrypt)); }else{ String
       * passdecrypt = AddcelCryptoRSA.decryptPrivate(usuarioVO.getUsrPwd());
       * LOGGER.info("pass decrypt colombia : " + passdecrypt); usuarioVO.setPassword(passdecrypt);
       * }
       */
//      String passencrypt = AddcelCrypto.encryptHard(usuarioVO.getUsrPwd());
//      LOGGER.info("passencrypt : "+passencrypt);
      String passdecrypt = AddcelCrypto.decryptHard(usuarioVO.getUsrPwd());
      String passmx = generaSemillaAux(passdecrypt);
      usuarioVO.setPassword(Crypto.aesEncrypt(passmx, passdecrypt));
      /*
       * String tarjeta = "1475785874745878"; tarjeta = gson.toJson(tarjeta); tarjeta =
       * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), tarjeta);
       * LOGGER.info("tarjeta encriptada : " + tarjeta); LOGGER.info("tarjeta desencriptada : " +
       * AddcelCrypto.decryptSensitive(tarjeta));
       */
      // usuarioVO.setPassword(AddcelCryptoRSA.encryptPublic(usuarioVO.getUsr_pwd()));
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_LOGIN + Constantes.LOG_FORM_USUARIO
          + usuarioVO.getUsrLogin());
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_LOGIN + Constantes.LOG_FORM_PASSWORD
          + usuarioVO.getUsrPwd());
      // if(password.length() <= 8){      
      status = dao.loginUsuario(usuarioVO);
      if (status == 0) {
        usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
        if (usuarioVO.getIdUsrStatus() >= 0) {
          if (usuarioVO.getIdError() == 0) {
            String card = UtilsService.getSMS(usuarioVO.getUsrTdcNumero());
            usuarioVO.setUsrTdcNumero(maskCard(card, "*"));
           /* usuarioVO.setUsrTdcNumero("XXXX XXXX XXXX "
                + usuarioVO.getUsrTdcNumero().substring(usuarioVO.getUsrTdcNumero().length() - 5,
                    usuarioVO.getUsrTdcNumero().length() - 1));*/
            /*
             * usuarioVO.setFechaNacimiento(UtilsService.cambiarFormatoFecha(
             * usuarioVO.getFechaNacimiento(), FORMATO_FECHA_ENTRADA_BASE, FORMATO_FECHA_SALIDA));
             */
            usuarioVO.setUsrPwd("");
            // json = getRespuesta(usuarioVO, status);
          } else {
            json =
                new StringBuffer().append("{\"idError\": -1").append(",\"mensajeError\":")
                    .append(usuarioVO.getMensajeError()).append("}").toString();
          }
          json = gson.toJson(usuarioVO);
        } else {
          json = gson.toJson(usuarioVO);
        }

      } else {
        usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
        json = gson.toJson(usuarioVO);
      }
      // } else {
      // json = new StringBuffer().append("{\"idError\":").append(-1).
      // append(",\"mensajeError\":\"El password debe tener una longitud minima de 8 digitos.\"").
      // append("}").toString();
      // }
    } catch (MalformedJsonException me) {
      json =
          new StringBuffer().append("{\"idError\":").append(-1).append(",\"mensajeError\":")
              .append("El password debe tener una longitud minima de 8 digitos.").append("}")
              .toString();
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // LOGGER.info("Finally");
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_CAMBIAR_TARJETA + usuarioVO.getUsrLogin());
    // LOGGER.info("json regresado : " + AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado : " + json);
    return json;
  }

  public String loginUsuarioEnc(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    parametroVO p1 = new parametroVO();
    try {
      String passwordb = "";
      String keyaux = "1234567890ABCDEF0123456789ABCDEF";
      LOGGER.info("loginUsuarioEnc");
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_LOGIN + usuarioVO.getUsrLogin());
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      p1.setParam(usuarioVO.getUsrLogin());
      /* Descomentar cuando se libere lo de colombia */
      // passwordb = dao.consultaPassword(p1);
      usuarioVO.setLogin(usuarioVO.getUsrLogin());
      /*
       * if(passwordb.length() <= 40){ String passdecrypt =
       * AddcelCryptoRSA.decryptPrivate(usuarioVO.getUsrPwd()); String passmx =
       * generaSemillaAux(passdecrypt); LOGGER.info("passmx : " + Crypto.aesEncrypt(passmx,
       * passdecrypt)); usuarioVO.setPassword(Crypto.aesEncrypt(passmx, passdecrypt)); }else{ String
       * passdecrypt = AddcelCryptoRSA.decryptPrivate(usuarioVO.getUsrPwd());
       * LOGGER.info("pass decrypt colombia : " + passdecrypt); usuarioVO.setPassword(passdecrypt);
       * }
       */
      String passdecrypt = AddcelCrypto.decryptHard(usuarioVO.getUsrPwd());
      String passmx = generaSemillaAux(passdecrypt);
      usuarioVO.setPassword(Crypto.aesEncrypt(passmx, passdecrypt));
      /*
       * String tarjeta = "1475785874745878"; tarjeta = gson.toJson(tarjeta); tarjeta =
       * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), tarjeta);
       * LOGGER.info("tarjeta encriptada : " + tarjeta); LOGGER.info("tarjeta desencriptada : " +
       * AddcelCrypto.decryptSensitive(tarjeta));
       */
      // usuarioVO.setPassword(AddcelCryptoRSA.encryptPublic(usuarioVO.getUsr_pwd()));
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_LOGIN + Constantes.LOG_FORM_USUARIO
          + usuarioVO.getUsrLogin());
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_LOGIN + Constantes.LOG_FORM_PASSWORD
          + usuarioVO.getUsrPwd());
      // if(password.length() <= 8){      
      status = dao.loginUsuario(usuarioVO);      
      if (status == 0) {        
        usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);        
        if (usuarioVO.getIdUsrStatus() >= 0) {          
          if (usuarioVO.getIdError() == 0) {            
            final String tdc = UtilsService.getSMS(usuarioVO.getUsrTdcNumero());
            usuarioVO.setUsrTdcNumero(tdc);
            usuarioVO.setUsrTdcNumero(AddcelCrypto.encryptHard(tdc));
            /*
             * usuarioVO.setFechaNacimiento(UtilsService.cambiarFormatoFecha(
             * usuarioVO.getFechaNacimiento(), FORMATO_FECHA_ENTRADA_BASE, FORMATO_FECHA_SALIDA));
             */
            usuarioVO.setUsrPwd("");
            // json = getRespuesta(usuarioVO, status);
          } else {
            json =
                new StringBuffer().append("{\"idError\": -1").append(",\"mensajeError\":")
                    .append(usuarioVO.getMensajeError()).append("}").toString();
          }
          json = gson.toJson(usuarioVO);
        } else {
          json = gson.toJson(usuarioVO);
        }

      } else {
        usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
        json = gson.toJson(usuarioVO);
      }
      // } else {
      // json = new StringBuffer().append("{\"idError\":").append(-1).
      // append(",\"mensajeError\":\"El password debe tener una longitud minima de 8 digitos.\"").
      // append("}").toString();
      // }
    } catch (MalformedJsonException me) {
      json =
          new StringBuffer().append("{\"idError\":").append(-1).append(",\"mensajeError\":")
              .append("El password debe tener una longitud minima de 8 digitos.").append("}")
              .toString();
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // LOGGER.info("Finally");
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_CAMBIAR_TARJETA + usuarioVO.getUsrLogin());
    // LOGGER.info("json regresado : " + AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado : " + json);
    return json;
  }


  /*
   * public String guardarUsuarioMatch(String json){ UsuarioVO usuarioVO = null; UsuarioDao dao =
   * null; String password = null; int status = 0; try{ json = AddcelCrypto.decryptSensitive(json);
   * usuarioVO = gson.fromJson(json, UsuarioVO.class); dao = (UsuarioDao) getBean(USUARIO_DAO);
   * password = UtilsService.generatePassword(10);
   * 
   * LOGGER.info(Constantes.LOG_SERVICE_GUARDA_USUARIO + " MATCH " + Constantes.LOG_FORM_USUARIO +
   * usuarioVO.getLogin()); LOGGER.info(Constantes.LOG_SERVICE_GUARDA_USUARIO + " MATCH " +
   * Constantes.LOG_FORM_PASSWORD);
   * 
   * usuarioVO.setPassword(AddcelCryptoRSA.encryptPublic(password));
   * usuarioVO.setTarjetaCif(usuarioVO.getTarjeta());
   * usuarioVO.setTarjeta(AddcelCryptoRSA.encryptPublic(usuarioVO.getTarjeta()));
   * usuarioVO.setVigencia(AddcelCryptoRSA.encryptPublic(usuarioVO.getVigencia()));
   * usuarioVO.setFechaNacimiento(UtilsService.cambiarFormatoFecha( usuarioVO.getFechaNacimiento(),
   * FORMATO_FECHA_ENTRADA, FORMATO_FECHA_SALIDA));
   * 
   * status = dao.guardarUsuario(usuarioVO); json = getRespuesta(usuarioVO, status);
   * 
   * LOGGER.info(Constantes.LOG_SERVICE_GUARDA_USUARIO + " MATCH " +
   * Constantes.LOG_FORM_ID_USUARIO+usuarioVO.getIdUsuario() +
   * Constantes.LOG_FORM_USUARIO+usuarioVO.getLogin()); if(usuarioVO.getIdUsuario() > 0){
   * if(usuarioVO.isUsuarioMigrado()){ sendMail(usuarioVO, password, MODULE_CAMB_PASS); }else{
   * sendMail(usuarioVO, password, MODULE_BIENVENIDO); } sendSms(usuarioVO, password,
   * MODULE_BIENVENIDO); } }catch(Exception e){ json = UtilsService.getError(101, e); }finally{
   * if(json != null){ json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()),
   * json); } } return json; }
   */

  public String actualizarUsuario(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    try {
      // json = AddcelCrypto.decryptSensitive(json);
      LOGGER.info("json descifrado : " + json);
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_ACTUALIZA_USUARIO + json);
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      /*
       * if(validaDataCredit(usuarioVO.getIdAplicacion())){ //TarjetaVO tarjeta =
       * dao.getUsuarioTarjeta(usuarioVO.getLogin()); if(tarjeta.getCedula() == null){
       * usuarioVO.setTarjeta(AddcelCryptoRSA.decryptPrivate(tarjeta.getNumTarjeta()));
       * if(!realizaValidacionDataCredit(usuarioVO)){ json =
       * Constantes.JSON_ERROR_VALIDA_DATACREDIT; json =
       * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); return json; } } }
       */
      usuarioVO.setPassword(usuarioVO.getLogin());
      usuarioVO.setFechaNacimiento(UtilsService.cambiarFormatoFecha(usuarioVO.getFechaNacimiento(),
          FORMATO_FECHA_ENTRADA, FORMATO_FECHA_SALIDA));
      String encriptado = AddcelCrypto.encryptHard(usuarioVO.getTarjeta());      
      String tarjetades = AddcelCrypto.decryptHard(usuarioVO.getTarjeta());
      String tardes = generaSemillaAux(tarjetades);
      String tarjeta = Crypto.aesEncrypt(tardes, tarjetades);
      LOGGER.info("tarjeta convertida a cifrado mex : " + Crypto.aesEncrypt(tardes, tarjetades));
      usuarioVO.setTarjeta(tarjeta);
      status = dao.actualizarUsuario(usuarioVO);
      // json = getRespuesta(usuarioVO, status);
      usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
      json = gson.toJson(usuarioVO);
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_ACTUALIZA_USUARIO + usuarioVO.getLogin());
    // LOGGER.info("json regresado en actualizacion de datos: " +
    // AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado en actualizacion de datos: " + json);
    return json;
  }


  public String actualizarPaisUsu(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    try {
      // json = AddcelCrypto.decryptSensitive(json);      
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      status = dao.actualizarPaisUsu(usuarioVO);
      // json = getRespuesta(usuarioVO, status);
      usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
      json = gson.toJson(usuarioVO);
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_ACTUALIZA_PAIS_USUARIO + usuarioVO.getLogin());
    // LOGGER.info("json regresado en actualizacion de datos: " +
    // AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado en actualizacion de usuario: " + json);
    return json;
  }

  public String actualizarNombreUsu(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    try {      
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_ACTUALIZA_NOMBRE_USUARIO + json);
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      status = dao.actualizarNombreUsu(usuarioVO);
      // json = getRespuesta(usuarioVO, status);
      usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
      json = gson.toJson(usuarioVO);
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_ACTUALIZA_USUARIO + usuarioVO.getLogin());
    // LOGGER.info("json regresado en actualizacion de datos: " +
    // AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado en actualizacion de usuario: " + json);
    return json;
  }


  public String actualizarUsrLogin(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    try {
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_ACTUALIZA_USUARIO + json);
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      status = dao.actualizarUsrLogin(usuarioVO);
      // json = getRespuesta(usuarioVO, status);
      usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
      json = gson.toJson(usuarioVO);
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_ACTUALIZA_USUARIO + usuarioVO.getLogin());
    // LOGGER.info("json regresado en actualizacion de datos: " +
    // AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado en actualizacion de usuario: " + json);
    return json;
  }

  public String actualizarCorreo(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    try {
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info("actualizacion de correo" + json);
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      status = dao.actualizarCorreo(usuarioVO);
      // json = getRespuesta(usuarioVO, status);
      usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
      json = gson.toJson(usuarioVO);
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_ACTUALIZA_USUARIO + usuarioVO.getLogin());
    // LOGGER.info("json regresado en actualizacion de datos: " +
    // AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado en actualizacion de correo: " + json);
    return json;
  }

  public String actualizarTelefono(String json) {
    TUsuarios usuarioVO = null;
    UsuarioDao dao = null;
    int status = 0;
    try {      
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info("actualizacion de telefono" + json);
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      status = dao.actualizarTelefono(usuarioVO);
      // json = getRespuesta(usuarioVO, status);
      usuarioVO = gson.fromJson(usuarioVO.getRespuesta(), TUsuarios.class);
      json = gson.toJson(usuarioVO);
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_ACTUALIZA_USUARIO + usuarioVO.getLogin());
    // LOGGER.info("json regresado en actualizacion de datos: " +
    // AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado en actualizacion de telefono: " + json);
    return json;
  }

  /*
   * public String bajaUsuario(String json) { UsuarioVO usuarioVO = null; UsuarioDao dao = null; int
   * status = 0; try{ json = AddcelCrypto.decryptSensitive(json); usuarioVO = gson.fromJson(json,
   * UsuarioVO.class); LOGGER.info(Constantes.LOG_SERVICE_INICIO_BAJA_USUARIO +
   * usuarioVO.getLogin()); dao = (UsuarioDao) getBean(USUARIO_DAO); status =
   * dao.bajaUsuario(usuarioVO); json = getRespuesta(usuarioVO, status);
   * 
   * }catch(Exception e){ json = UtilsService.getError(101, e); }finally{ if(json != null){ json =
   * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); } }
   * LOGGER.info(Constantes.LOG_SERVICE_FIN_BAJA_USUARIO + usuarioVO.getLogin()); return json; }
   */

  public String cambiarPassword(String json) {

    TUsuarios usuarioVO = null;
    List<TUsuarios> usuarioFind = null;
    UsuarioDao dao = null;
    String password = null;
    int status = 0;
    String idi = "";
    String usuario = "";
    parametroVO p1 = new parametroVO();
    try {
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_CAMBIAR_PASSWORD + usuarioVO.getUsrLogin());
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      idi = usuarioVO.getIdioma();
      usuario = usuarioVO.getUsrLogin();
      /*solo para la prueba*/
//      String pass = AddcelCrypto.encryptHard(usuarioVO.getUsrPwd());
//      LOGGER.info("password actual encriptado: "+pass);
//      	String newpass = AddcelCrypto.encryptHard(usuarioVO.getNewPassword());
//      	LOGGER.info("password actual encriptado: "+newpass);
      /*fin prueba*/
      /* convertirmos el password actual del usuario con Crypto como en mexico */
      String passactual = AddcelCrypto.decryptHard(usuarioVO.getUsrPwd());      
      /* convertimos el pass actual a como esta en mexico */
      String passactsem = generaSemillaAux(passactual);
      String passactconv = Crypto.aesEncrypt(passactsem, passactual);
      usuarioVO.setPassword(passactconv);
      password = AddcelCrypto.decryptHard(usuarioVO.getNewPassword());
      usuarioVO.setLogin(usuarioVO.getUsrLogin());
      String passaux = generaSemillaAux(password);
      String passnew = Crypto.aesEncrypt(passaux, password);
      usuarioVO.setNewPassword(passnew);
      usuarioVO.setIdioma(usuarioVO.getIdioma());
      // LOGGER.info("login que se manda : " + usuarioVO.getLogin());
      status = dao.cambiarPasswordUsuario(usuarioVO);
      json = getRespuesta(usuarioVO, status);
      p1.setParam(usuario);
      String email = dao.consultaeMailUsuario(p1);
      if (status == 0 && usuarioVO.getIdError() == 0) {
        usuarioFind = dao.getUsuarios(usuarioVO);
        if (usuarioFind != null && usuarioFind.size() > 0) {
          if (idi.equals("es")) {
            if (UtilsService.patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*") ) {              
              SendMailMicrosoft(email, "@MENSAJE_RECOVERYPWD_TELECOM_ES", "@ASUNTO_RECOVERYPWD_TELECOM_ES",
                  usuario, password);
            } else {              
              SendMailNormal(email, "@MENSAJE_RECOVERYPWD_TELECOM_ES", "@ASUNTO_RECOVERYPWD_TELECOM_ES",
                      usuario, password);
            }
          } else if (UtilsService.patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*")  ) {            
            SendMailMicrosoft(email, "@MENSAJE_RECOVERYPWD_TELECOM_EN", "@ASUNTO_RECOVERYPWD_TELECOM_EN", usuario,
                password);
          } else {            
            SendMailNormal(email, "@MENSAJE_RECOVERYPWD_TELECOM_EN", "@ASUNTO_RECOVERYPWD_TELECOM_EN", usuario,
                    password);
          }

        }
      }
    } catch (Exception e) {
      json = UtilsService.getError(101, e);
    } finally {
      if (json != null) {
        // json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_CAMBIAR_PASSWORD + usuarioVO.getLogin());
    // LOGGER.info("json regresado en actualizacion password: " +
    // AddcelCrypto.decryptSensitive(json));
    LOGGER.info("json regresado en actualizacion password: " + json);
    return json;
  }
  
  /*PRUEBA para telecom*/
  public String resetPassword2(String json) {
	  
	  
	  LOGGER.info("resetPassword");
	  TUsuarios usuarioVO = null;
	  UsuarioDao dao = null;
	  String email = null;
	  String password = null;
	  String usuario = null;
	  String codigo;
	  String idi = null;
	  String keyaux = "1234567890ABCDEF0123456789ABCDEF";
	  String cpAmex = null;
	  String domAmex = null;
	  String ciudad = null;
	  String fechaNac = null;
	  String ssn = null;
	  
	  
	  LOGGER.info("Json: "+json);
	  wallet = new WalletService();
  	  LOGGER.debug("iniciando wallet");
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      email = usuarioVO.geteMail();
      usuario = usuarioVO.getUsrLogin();
      idi = usuarioVO.getIdioma();
      codigo = usuarioVO.getUsrTdcCodigo();
      cpAmex = usuarioVO.getUsrCp();
      domAmex = usuarioVO.getUsrDomAmex();
      ciudad = usuarioVO.getUsrCiudad();      
      fechaNac =usuarioVO.getUsrFechaNac();
      ssn =usuarioVO.getUsrNss();
      String estado = "CA";
      String govtId = "D5023674";
      String govtIdExpirationDate = "03012020";
      String govtIdIssueDate = "02072014";
      String govtIdIssueState = "CA";
      
      
      
      
//      int id_tarjeta = Integer.parseInt(usuarioVO.getTarjeta());      
//      String deshardtar = AddcelCrypto.decryptHard(usuarioVO.getUsrTdcNumero());      
//      String deshardvigencia = AddcelCrypto.decryptHard(usuarioVO.getUsrTdcVigencia());
      password = usuarioVO.getUsrPwd();
      
      TUsuarios usuariologin = null;
      LOGGER.info("AddcelCrypto.encryptHard(password): "+AddcelCrypto.encryptHard(password));
      String login = loginUsuario("{\"idioma\":\"" +idi + "\",\"usrLogin\":\"" + usuario + "\",\"usrPwd\":\"" +AddcelCrypto.encryptHard(password) + "\"}");
      LOGGER.debug("respuesta login: " + login);
      usuariologin = gson.fromJson(login, TUsuarios.class);
      

      
//  	  wallet.addCardTelecom(usuariologin.getUsrNombre(), AddcelCrypto.encryptHard(deshardtar), AddcelCrypto.encryptHard(deshardvigencia), usuariologin.getIdeUsuario(), id_tarjeta, cpAmex, domAmex, false);
      
      
  	  
      wallet.addCardTelecom(usuariologin.getIdeUsuario(),cpAmex,domAmex,ciudad,fechaNac,idi,ssn,govtIdIssueState, govtIdIssueDate, govtIdExpirationDate, govtId, estado);
      
//      wallet.addCard(codigo, true, idi, usuariologin.getUsrNombre(), AddcelCrypto.encryptHard(deshardtar), AddcelCrypto.encryptHard(deshardvigencia), usuariologin.getIdeUsuario(),cpAmex,domAmex,false);
	  return json;
  }
  
  
  
//  public String resetPassword(String json) {
//	  
//	TUsuarios usuarioVO = null;  
//    LOGGER.info("json : " + json);
//    usuarioVO = gson.fromJson(json, TUsuarios.class);
//    String idioma = usuarioVO.getIdioma();
//    long id_usuario = usuarioVO.getIdeUsuario();
//    LOGGER.info("idioma: "+idioma);
//    LOGGER.info("id_usuario: "+id_usuario);
//    
//  	wallet = new WalletService();
//  	LOGGER.debug("iniciando wallet");
//  	wallet.getCard(idioma, id_usuario);
//  	return json;
//	  
//  }

  public String resetPassword(String json) {
    TUsuarios usuarioVO = null;
    AbstractVO resp = new AbstractVO();
    List<TUsuarios> usuarioFind = null;
    String json2 = "";
    UsuarioDao dao = null;
    int status = 0;
    String password = null;
    String idi = "";
    String usuario = "";
    parametroVO p1 = new parametroVO();
    try {
      usuarioVO = gson.fromJson(json, TUsuarios.class);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_RESET_PASSWORD + usuarioVO.getUsrLogin());
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      idi = usuarioVO.getIdioma();
      usuario = usuarioVO.getUsrLogin();
      password = UtilsService.generatePassword(8);      
      String passr = generaSemillaAux(password);
      String passrc = Crypto.aesEncrypt(passr, password);
      // LOGGER.info("nuevo password sin cifrar : " + password);
      usuarioVO.setUsrPwd(passrc);
      resp.setLogin(usuarioVO.getUsrLogin());
      resp.setPassword(passrc);
      resp.setIdioma(usuarioVO.getIdioma());
      status = dao.resetPasswordUsuario(resp);
      LOGGER.info("status : " + status);
      LOGGER.info("respuesta : " + resp.getRespuesta());
      resp = gson.fromJson(resp.getRespuesta(), AbstractVO.class);
      json2 = gson.toJson(resp);
      p1.setParam(usuario);
      String email = dao.consultaeMailUsuario(p1);
      
      if (status == 0 && resp.getIdError() == 0) {
        usuarioFind = dao.getUsuarios(usuarioVO);
        if (usuarioFind != null && usuarioFind.size() > 0) {
          if (idi.equals("es")) {
            if (UtilsService.patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*")) {              
              SendMailMicrosoft(email, "@MENSAJE_RECOVERYPWD_TELECOM_ES", "@ASUNTO_RECOVERYPWD_TELECOM_ES",
                  usuario, password);
            } else {                            
              SendMailNormal(email, "@MENSAJE_RECOVERYPWD_TELECOM_ES", "@ASUNTO_RECOVERYPWD_TELECOM_ES",
                      usuario, password);
            }
          } else if (UtilsService.patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*")) {            
            SendMailMicrosoft(email, "@MENSAJE_RECOVERYPWD_TELECOM_EN", "@ASUNTO_RECOVERYPWD_TELECOM_EN", usuario,
                password);
          } else {            
            SendMailNormal(email, "@MENSAJE_RECOVERYPWD_TELECOM_EN", "@ASUNTO_RECOVERYPWD_TELECOM_EN", usuario,
                    password);
          }
        }
      }
    } catch (Exception e) {
      json2 = UtilsService.getError(101, e);
    } finally {
      if (json2 != null) {
        // json2 = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json2);
      }
    }
    LOGGER.info(Constantes.LOG_SERVICE_FIN_RESET_PASSWORD + usuarioVO.getUsrLogin());
    // LOGGER.info("json regresado en actualizacion de datos: " +
    // AddcelCrypto.decryptSensitive(json2));
    LOGGER.info("json regresado en reset password: " + json2);
    return json2;
  }

  /*
   * public String cambiarTarjeta(String json){ UsuarioVO usuarioVO = null; UsuarioVO usuarioLogin =
   * null; UsuarioDao dao = null; int status = 0; try{ json = AddcelCrypto.decryptSensitive(json);
   * usuarioVO = gson.fromJson(json, UsuarioVO.class);
   * LOGGER.info(Constantes.LOG_SERVICE_INICIO_CAMBIAR_TARJETA + usuarioVO.getLogin()); dao =
   * (UsuarioDao) getBean(USUARIO_DAO);
   * usuarioVO.setPassword(AddcelCryptoRSA.encryptPublic(usuarioVO.getPassword()));
   * usuarioVO.setLogin(usuarioVO.getLogin()); dao.loginUsuario(usuarioVO); usuarioLogin =
   * gson.fromJson(usuarioVO.getRespuesta(), UsuarioVO.class); if(usuarioLogin.getIdError() == 0){
   * if(usuarioLogin.getCedula() == null && StringUtils.EMPTY.equals(usuarioLogin.getCedula())){
   * json = Constantes.JSON_ERROR_NO_CEDULA; json =
   * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); return json; }
   * usuarioLogin.setIdAplicacion(usuarioVO.getIdAplicacion());
   * usuarioLogin.setTarjeta(usuarioVO.getTarjeta());
   * usuarioLogin.setVigencia(usuarioVO.getVigencia());
   * if(validaDataCredit(usuarioVO.getIdAplicacion())){
   * if(!realizaValidacionDataCredit(usuarioLogin)){ json = Constantes.JSON_ERROR_VALIDA_DATACREDIT;
   * json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); return json; }
   * } usuarioVO.setTarjetaCif(AddcelCryptoRSA.encryptPublic(usuarioVO.getTarjeta()));
   * usuarioVO.setVigencia(AddcelCryptoRSA.encryptPublic(usuarioVO.getVigencia())); status =
   * dao.cambiarTarjetaUsuario(usuarioVO); json = getRespuesta(usuarioVO, status); } else { json =
   * usuarioLogin.getRespuesta(); } }catch(Exception e){ json = UtilsService.getError(101, e);
   * }finally{ json = AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); }
   * LOGGER.info(Constantes.LOG_SERVICE_FIN_CAMBIAR_TARJETA + usuarioVO.getLogin()); return json; }
   */

  /*
   * public String procesaTarjetaUsuario(String json) { MovimientosTarjetaVO tarjetasVO = null;
   * TarjetaVO[] tarjetasArray = null; List<TarjetaVO> tarjetasList = null; UsuarioDao dao = null;
   * AbstractVO res = null; try { json = AddcelCrypto.decryptSensitive(json); tarjetasVO =
   * gson.fromJson(json, MovimientosTarjetaVO.class);
   * LOGGER.info(Constantes.LOG_INICIO_PROCESA_TARJETAS_USUARIO
   * +tarjetasVO.getLogin()+" - "+tarjetasVO.getTipoMovimiento());
   * LOGGER.info(Constantes.LOG_INICIO_PROCESA_TARJETAS_JSON+json); dao = (UsuarioDao)
   * getBean(USUARIO_DAO); if(tarjetasVO.getTipoMovimiento() == 1){ UsuarioVO usuario = new
   * UsuarioVO(); usuario.setLogin(tarjetasVO.getLogin());
   * usuario.setPassword(AddcelCryptoRSA.encryptPublic(tarjetasVO.getPassword())); int status =
   * dao.loginUsuario(usuario); if(status == 0){ usuario = gson.fromJson(usuario.getRespuesta(),
   * UsuarioVO.class); usuario.setIdAplicacion(tarjetasVO.getIdAplicacion());
   * usuario.setTarjeta(tarjetasVO.getTarjeta()); usuario.setVigencia(tarjetasVO.getVigencia());
   * if(validaDataCredit(tarjetasVO.getIdAplicacion())){ if(!realizaValidacionDataCredit(usuario)){
   * json = Constantes.JSON_ERROR_VALIDA_DATACREDIT; json =
   * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); return json; } } } }
   * tarjetasVO.setPassword(AddcelCryptoRSA.encryptPublic(tarjetasVO.getPassword()));
   * tarjetasVO.setTarjetaCifr(AddcelCryptoRSA.encryptPublic(tarjetasVO.getTarjeta()));
   * tarjetasVO.setVigencia(AddcelCryptoRSA.encryptPublic(tarjetasVO.getVigencia())); json =
   * dao.procesaTarjetaUsuario(tarjetasVO);
   * 
   * if(tarjetasVO.getTipoMovimiento() == 3){
   * LOGGER.info(Constantes.LOG_CIFRANDO_TARJETAS+tarjetasVO.getLogin()); tarjetasArray =
   * gson.fromJson(json, TarjetaVO[].class); tarjetasList = new ArrayList<TarjetaVO>(); for(int i =
   * 0; i < tarjetasArray.length; i++){ TarjetaVO tarjeta = tarjetasArray[i];
   * tarjeta.setNumTarjeta(AddcelCryptoRSA.decryptPrivate(tarjeta.getNumTarjeta()));
   * tarjeta.setNumTarjeta("XXXX XXXX XXXX " +
   * tarjeta.getNumTarjeta().substring(tarjeta.getNumTarjeta().length() - 4
   * ,tarjeta.getNumTarjeta().length())); tarjetasList.add(tarjeta); } json =
   * gson.toJson(tarjetasList); } else { res = gson.fromJson(json, AbstractVO.class); json =
   * gson.toJson(res); } } catch (Exception e) { json =
   * Constantes.ERROR_JSON_PROCESA_TARJETAS_USUARIO; e.printStackTrace(); }finally{
   * LOGGER.info(Constantes.LOG_RESPUESTA_PROCESA_TARJETAS_USUARIO +
   * tarjetasVO.getLogin()+" - "+json); json =
   * AddcelCrypto.encryptSensitive(FORMATO_CIFRADO.format(new Date()), json); }
   * LOGGER.info(Constantes
   * .LOG_FIN_PROCESA_TARJETAS_USUARIO+tarjetasVO.getLogin()+" - "+tarjetasVO.getTipoMovimiento());
   * return json; }
   */

  private String getRespuesta(TUsuarios usuarioVO, int statusBD) {
    UsuarioResponse respuesta = new UsuarioResponse();
    String json = null;
    long status = 0;
    if (statusBD == 0) {
      String[] respIns = ((TUsuarios) usuarioVO).getRespuesta().split(";");
      status = new Long(respIns[0]);
      if (status >= 0) {
        respuesta.setLogin(usuarioVO.getUsrLogin());
        if (status > 0) {
          respuesta.setIdUsuario(status);
          usuarioVO.setIdUsuario(status);
        }
        respuesta.setMensajeError(respIns[1]);
      } else {
        respuesta.setIdError((int) status);
        respuesta.setMensajeError(respIns[1]);
      }
      usuarioVO.setIdError((int) status);
    } else {
      respuesta.setIdError(statusBD);
      respuesta.setMensajeError("Ocurrio un error en la BD.");
    }
    json = gson.toJson(respuesta);
    LOGGER.info(Constantes.LOG_SERVICE_RESPUESTA_JSON + json);
    return json;
  }


  private static final String MODULE_BIENVENIDO = "Bienvenido";
  private static final String MODULE_CAMB_PASS = "Cambioclave";

  private void sendMail(TUsuarios usuarioVO, String password, String idModule) {
    MailSenderWSProxy mailSenderWSProxy = new MailSenderWSProxy();
    String json = null;
    try {
      CorreoVO correo = new CorreoVO();
      HashMap<String, String> tags = new HashMap<String, String>();
      String[] to = {usuarioVO.getEmail()};
      String[] cid = {};
      correo.setBcc(new String[] {});
      correo.setCc(new String[] {});
      correo.setCid(cid);
      correo.setTo(to);
      correo.setIdAplicacion(usuarioVO.getIdAplicacion());
      correo.setIdModulo(idModule);
      tags.put("<#NOMBRE#>", usuarioVO.getNombres());
      tags.put("<#PASSWORD#>", password);
      correo.setTags(tags);
      json = gson.toJson(correo);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_ENVIO_MAIL + json);
      json = mailSenderWSProxy.enviaCorreoMandrill(json);
      LOGGER.info(Constantes.LOG_SERVICE_RESPUESTA_ENVIO_MAIL + json);
    } catch (Exception e) {
      LOGGER.info(Constantes.LOG_SERVICE_ERROR_ENVIO_MAIL + e);
    }
  }

  private void sendSms(TUsuarios usuarioVO, String password, String idModule) {
    MailSenderWSProxy mailSenderWSProxy = new MailSenderWSProxy();
    String json = null;
    try {
      SmsVO smsVO = new SmsVO();
      HashMap<String, String> tags = new HashMap<String, String>();
      smsVO.setNumero(usuarioVO.getUsrTelefono());
      // smsVO.setIdAplicacion(usuarioVO.getIdAplicacion());
      smsVO.setIdModulo(idModule);
      tags.put("<#PASSWORD#>", password);
      smsVO.setTags(tags);
      json = gson.toJson(smsVO);
      LOGGER.info(Constantes.LOG_SERVICE_INICIO_ENVIO_SMS + json);
      json = mailSenderWSProxy.envioSMS(json);
      LOGGER.info(Constantes.LOG_SERVICE_RESPUESTA_ENVIO_SMS + json);
    } catch (Exception e) {
      LOGGER.info(Constantes.LOG_SERVICE_ERROR_ENVIO_SMS + e);
    }
  }

  /*
   * private boolean realizaValidacionDataCredit(UsuarioVO usuarioVO) { String xmlRequest = null;
   * String response = null; UsuarioDao dao = null; try { dao = (UsuarioDao) getBean(USUARIO_DAO);
   * ExperianUserVO user = dao.getDatosExperianUsers(usuarioVO.getIdAplicacion()); user =
   * gson.fromJson(user.getRespuesta(), ExperianUserVO.class); ServicioValidacionImplProxy
   * proxyDataCredit = new ServicioValidacionImplProxy(); xmlRequest =
   * "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<ValidadorCuenta>" +
   * "<Cuenta tipoIdUsuario=\""+user.getTipoIdUsuario()+"\" " +
   * "idUsuario=\""+user.getIdUsuario()+"\" " + "nitUsuario=\""+user.getNitUsuario()+"\" " +
   * "clave=\""+user.getClave()+"\" " + "tipoIdCliente=\""+user.getTipoIdCliente()+"\" " +
   * "idCliente=\""+usuarioVO.getCedula()+"\" " + "ctaCliente=\""+usuarioVO.getTarjeta()+"\" " +
   * "primerApellido=\""
   * +usuarioVO.getApellidoPrim().trim()+" "+usuarioVO.getApellidoSeg().trim()+"\"" +
   * " nombreCliente=\""+usuarioVO.getNombres().trim()+"\" /></ValidadorCuenta>";
   * LOGGER.info(Constantes.LOG_INICIO_COMUNICACION_DATACREDIT+"["+usuarioVO.getLogin()+"]");
   * LOGGER.info(Constantes.LOG_DATACREDIT_DATOS+usuarioVO.getLogin());
   * LOGGER.info(Constantes.LOG_DATACREDIT_XML+xmlRequest); response =
   * proxyDataCredit.valCuenta(xmlRequest); Document doc = convertStringToDocument(response,
   * usuarioVO); String codigoRespuesta = getCodigoRespuestaDocument(doc, usuarioVO);
   * LOGGER.info(Constantes.LOG_FIN_COMUNICACION_DATACREDIT+" ["+usuarioVO.getLogin()+"]");
   * LOGGER.info
   * (Constantes.LOG_DATACREDIT_RESPUESTA+"["+usuarioVO.getLogin()+"]- Respuesta de Validacion: "
   * +response);
   * LOGGER.info(Constantes.LOG_DATACREDIT_CODIGO_RESP+"["+usuarioVO.getLogin()+"]"+codigoRespuesta
   * ); if("00".equals(usuarioVO.getCodigoRespuestaDC())){ return true; } } catch (Exception e) {
   * LOGGER.error(Constantes.LOG_ERROR_COMUNICACION_DATACREDIT+"["+usuarioVO.getLogin()+"] ", e); }
   * return false; }
   */

  private String getCodigoRespuestaDocument(Document doc, UsuarioVO usuarioVO) {
    String resp = null;
    if (doc.hasChildNodes()) {
      resp = printNote(doc.getChildNodes(), usuarioVO);
    }
    return resp;
  }

  private String printNote(NodeList nodeList, UsuarioVO usuarioVO) {
    LOGGER.debug(Constantes.LOG_DATACREDIT_VALIDANDO_RESPUESTA + usuarioVO.getLogin());
    String codResp = null;
    for (int count = 0; count < nodeList.getLength(); count++) {
      Node tempNode = nodeList.item(count);
      if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
        LOGGER.debug(Constantes.LOG_DATACREDIT_NOMBRE_NODO_RESP + tempNode.getNodeName());
        LOGGER.debug(Constantes.LOG_DATACREDIT_VALOR_NODO_RESP + tempNode.getTextContent());
        if (tempNode.hasAttributes()) {
          NamedNodeMap nodeMap = tempNode.getAttributes();
          for (int i = 0; i < nodeMap.getLength(); i++) {
            Node node = nodeMap.item(i);
            LOGGER.debug(Constantes.LOG_DATACREDIT_NOMBRE_ATRIBUTO_RESP + node.getNodeName());
            LOGGER.debug(Constantes.LOG_DATACREDIT_VALOR_ATRIBUTO_RESP + node.getNodeValue());
            if (CODIGO_RESPUESTA_DATACREDIT.equals(node.getNodeName())) {
              codResp = node.getNodeValue();
              usuarioVO.setCodigoRespuestaDC(codResp);
              break;
            }
          }
        }
        if (tempNode.hasChildNodes() && codResp == null) {
          printNote(tempNode.getChildNodes(), usuarioVO);
        }
      }
    }
    return codResp;
  }

  private Document convertStringToDocument(String xmlRequest, UsuarioVO usuarioVO) {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    try {
      builder = factory.newDocumentBuilder();
      Document doc = builder.parse(new InputSource(new StringReader(xmlRequest)));
      if (doc.hasChildNodes()) {
        printNote(doc.getChildNodes(), usuarioVO);
      }
      LOGGER.info(Constantes.LOG_DATACREDIT_DOCUMENT + doc);
      return doc;
    } catch (Exception e) {
      LOGGER.info(Constantes.LOG_ERROR_VALIDACION_DATACREDIT, e);
    }
    return null;
  }

  private boolean validaDataCredit(int idAplicacion) {
    UsuarioDao dao = null;
    try {
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      int validacionDataCredit = dao.validaDataCredit(idAplicacion);
      if (validacionDataCredit != 0) {
        return true;
      }
    } catch (Exception e) {
      LOGGER.error(Constantes.LOG_ERROR_DAO_VALIDACION_DATA_CREDIT + idAplicacion + " " + e);
    }
    return false;
  }

  private String iniciaEnrolamientoGemalto(UsuarioVO usuarioVO, String json) {
    LOGGER.info(Constantes.LOG_INICIO_ENROLAMIENTO_GEMALTO + usuarioVO.getLogin());
    if (tieneValidacionGemalto(usuarioVO.getIdAplicacion())) {
      UserRepositoryWSSoapProxy userRepositoryWS = new UserRepositoryWSSoapProxy();
      BooleanHolder result = new BooleanHolder();
      StringHolder returnMessage = new StringHolder();
      try {
        LOGGER.info(Constantes.LOG_INICIO_COMUNICACION_GEMALTO + "[USER REPOSITORY WS - "
            + usuarioVO.getLogin() + "]");
        userRepositoryWS.addUser(usuarioVO.getLogin(), usuarioVO.getLogin(), usuarioVO.getEmail(),
            usuarioVO.getTelefono(), usuarioVO.getPassword(), result, returnMessage);
        LOGGER.info(Constantes.LOG_FIN_COMUNICACION_GEMALTO + "[USER REPOSITORY WS - "
            + usuarioVO.getLogin() + "]");
        LOGGER.info(Constantes.LOG_GEMALTO_RESPUESTAS + "[USER REPOSITORY WS - "
            + usuarioVO.getLogin() + " - " + returnMessage.value + "]");
        // actualizaUsuarioEnrolamiento(usuarioVO);
        crearEnrolamientoGemalto(usuarioVO);
        UsuarioResponse usuarioResponse = gson.fromJson(json, UsuarioResponse.class);
        usuarioResponse.setActivationCodeGemalto(usuarioVO.getActivationCodeGemalto());
        usuarioResponse.setSerialNumberGemalto(usuarioVO.getSerialNumberGemalto());
        usuarioResponse.setTokenAlias(usuarioVO.getTokenAlias());
        json = gson.toJson(usuarioResponse);
        LOGGER.info(Constantes.LOG_RESPUESTA_ENROLAMIENTO_GEMALTO + json);
        return json;
      } catch (RemoteException e) {
        LOGGER.info(Constantes.LOG_ERROR_VALIDACION_GEMALTO + "[" + usuarioVO.getLogin() + "]" + e);
      } catch (Exception e) {
        LOGGER.info(Constantes.LOG_ERROR_VALIDACION_GEMALTO + "[" + usuarioVO.getLogin() + "]" + e);
      }
    }
    LOGGER.info(Constantes.LOG_FIN_ENROLAMIENTO_GEMALTO + usuarioVO.getLogin());
    return json;
  }

  /*
   * private void actualizaUsuarioEnrolamiento(UsuarioVO usuarioVO) { UsuarioDao dao = null; try {
   * dao = (UsuarioDao) getBean("UsuarioDao");
   * dao.actualizaUsuarioEnrolamiento(usuarioVO.getIdUsuario()); } catch (Exception e) {
   * LOGGER.info(Constantes.LOG_ERROR_VALIDACION_GEMALTO+"["+usuarioVO.getLogin()+"]"+e); } }
   */

  private boolean tieneValidacionGemalto(int idAplicacion) {
    UsuarioDao dao = null;
    try {
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      int res = dao.gemaltoOtp(idAplicacion);
      if (res != 0)
        return true;
    } catch (Exception e) {
      LOGGER.info(Constantes.LOG_ERROR_VALIDACION_GEMALTO_IDAPLICACION + "[" + idAplicacion + "]"
          + e);
    }
    return false;
  }

  private void crearEnrolamientoGemalto(UsuarioVO usuarioVO) {
    UsuarioDao dao = null;
    GemaltoDataVO gemaltoVO = null;
    try {
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      gemaltoVO = dao.getDataGemaltoServicio(1);
      gemaltoVO = gson.fromJson(gemaltoVO.getRespuesta(), GemaltoDataVO.class);
      creaUserGemaltoAdmin(usuarioVO, gemaltoVO);
      gemaltoVO = dao.getDataGemaltoServicio(2);
      gemaltoVO = gson.fromJson(gemaltoVO.getRespuesta(), GemaltoDataVO.class);
      creaUserToken(usuarioVO, gemaltoVO);
    } catch (Exception e) {
      LOGGER.info(Constantes.LOG_ERROR_VALIDACION_GEMALTO + "[" + usuarioVO.getLogin() + "]" + e);
    }
  }

  private void creaUserToken(UsuarioVO usuarioVO, GemaltoDataVO gemaltoVO) {
    StringHolder serialNumber = new StringHolder();
    StringHolder activationCode = new StringHolder();
    StringHolder createdTokenAlias = new StringHolder();
    IntHolder createUserTokenResult = new IntHolder();
    try {
      LOGGER.info(Constantes.LOG_INICIO_COMUNICACION_GEMALTO + "[ADMIN WS - CREATE USER - "
          + usuarioVO.getLogin() + "]");
      AdminWS2SoapProxy adminProxy = new AdminWS2SoapProxy();
      adminProxy.createUserToken(gemaltoVO.getFromDomainMnemonic(), gemaltoVO.getFromUser(),
          gemaltoVO.getToDomainMnemonic(), usuarioVO.getLogin(), gemaltoVO.getProfileName(),
          gemaltoVO.getTokenAlias(), createUserTokenResult, serialNumber, activationCode,
          createdTokenAlias);
      LOGGER.info(Constantes.LOG_FIN_COMUNICACION_GEMALTO + "[[ADMIN WS - CREATE USER - "
          + usuarioVO.getLogin() + "]");
      LOGGER.info(Constantes.LOG_GEMALTO_RESPUESTAS + "[ADMIN WS - CREATE USER - "
          + usuarioVO.getLogin() + " - " + activationCode.value + "]");
      LOGGER.info(Constantes.LOG_GEMALTO_RESPUESTAS + "[ADMIN WS - CREATE USER - "
          + usuarioVO.getLogin() + " - " + serialNumber.value + "]");
      LOGGER.info(Constantes.LOG_GEMALTO_RESPUESTAS + "[ADMIN WS - CREATE USER - "
          + usuarioVO.getLogin() + " - " + createdTokenAlias.value + "]");
      usuarioVO.setActivationCodeGemalto(activationCode.value);
      usuarioVO.setSerialNumberGemalto(serialNumber.value);
      usuarioVO.setTokenAlias(createdTokenAlias.value);
    } catch (Exception e) {
      LOGGER.info(Constantes.LOG_ERROR_VALIDACION_GEMALTO + "[ADMIN WS - CREATE USER -"
          + usuarioVO.getLogin() + "]" + e);
    }
  }

  private void creaUserGemaltoAdmin(UsuarioVO usuarioVO, GemaltoDataVO gemaltoVO) {
    try {
      LOGGER.info(Constantes.LOG_INICIO_COMUNICACION_GEMALTO + "[ADMIN WS - ADD USER REPOSITORY - "
          + usuarioVO.getLogin() + "]");
      AdminWS2SoapProxy adminProxy = new AdminWS2SoapProxy();
      AddUserResponseAddUserResult addUserResult =
          adminProxy.addUser(gemaltoVO.getFromDomainMnemonic(), gemaltoVO.getFromUser(),
              gemaltoVO.getToDomainMnemonic(), usuarioVO.getLogin(), null);
      LOGGER.info(Constantes.LOG_FIN_COMUNICACION_GEMALTO + "[[ADMIN WS - ADD USER REPOSITORY - "
          + usuarioVO.getLogin() + "]");
      LOGGER.info(Constantes.LOG_GEMALTO_RESPUESTAS + "[ADMIN WS - ADD USER REPOSITORY - "
          + usuarioVO.getLogin() + " - " + addUserResult.toString() + "]");
    } catch (Exception e) {
      LOGGER.info(Constantes.LOG_ERROR_VALIDACION_GEMALTO + "[ADMIN WS - ADD USER REPOSITORY -"
          + usuarioVO.getLogin() + "]" + e);
    }
  }

  public boolean SendMailMicrosoft(String sMail, String MessageType, String subject, String user,
      String pass) {
    boolean flag = false;
    UsuarioDao dao = null;
    parametroVO p1 = new parametroVO();
    parametroVO p2 = new parametroVO();
    try {
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      p1.setParam(subject);
      p2.setParam(MessageType);
      String wAsunto = dao.consultaParametros(p1);
      String Cadena = dao.consultaParametros(p2);

      Cadena = Cadena.replace("@NOMBRE", "");
      Cadena = Cadena.replace("@USUARIO", user);
      Cadena = Cadena.replace("@PASSWORD", pass);

    /*  CorreoVO correo = new CorreoVO();
      String from = "no-reply@addcel.com";
      String[] to = {sMail};
      correo.setCc(new String[] {});
      correo.setBody(Cadena);
      correo.setFrom(from);
      correo.setSubject(wAsunto);
      correo.setTo(to);*/
      
      MailSender correo = new MailSender();
      String from = "no-reply@addcel.com";
      String[] to = {sMail};
      correo.setCc(new String[] {});
      correo.setBody(Cadena);
      correo.setFrom(from);
      correo.setSubject(wAsunto);
      correo.setTo(to);
      
      Gson gson = new Gson();
      String json = gson.toJson(correo);
      LOGGER.info("antes de enviar mail");
      UtilsService.MailSender(json);
     // enviarCorreoMicrosoft(correo);
      LOGGER.info("despues de enviar mail");
      LOGGER.debug("mensaje hotmail enviado : " + sMail);
    } catch (Exception ex) {
      LOGGER.error("Ocurrio un error al enviar email hotmail {}:  {}", sMail, ex);
    }
    return flag;
  }

  
  

  public boolean SendMailNormal(String sMail, String MessageType, String subject, String user,
      String pass) {
    boolean flag = false;
    UsuarioDao dao = null;
    parametroVO p1 = new parametroVO();
    parametroVO p2 = new parametroVO();
    try {
      dao = (UsuarioDao) getBean(USUARIO_DAO);
      // LOGGER.info("debug 1" + subject);
      p1.setParam(subject);
      // LOGGER.info("debug 2" + MessageType);
      p2.setParam(MessageType);
      // LOGGER.info("debug 3");
      String wAsunto = dao.consultaParametros(p1);
      // LOGGER.info("debug 4" + wAsunto);
      String Cadena = dao.consultaParametros(p2);
      // LOGGER.info("debug 5" + Cadena);

      Cadena = Cadena.replace("@NOMBRE", "");
      Cadena = Cadena.replace("@USUARIO", user);
      Cadena = Cadena.replace("@PASSWORD", pass);

      CorreoVO correo = new CorreoVO();
      String from = "no-reply@addcel.com";
      String[] to = {sMail};
      correo.setCc(new String[] {});
      correo.setBody(Cadena);
      correo.setFrom(from);
      correo.setSubject(wAsunto);
      correo.setTo(to);
      Gson gson = new Gson();
      String json = gson.toJson(correo);
      LOGGER.info("antes de enviar mail");
      enviarCorreo(correo);
      LOGGER.info("despues de enviar mail");
      LOGGER.debug("mensaje SendMailNormal enviado : " + sMail);
    } catch (Exception ex) {
      LOGGER.error("Ocurrio un error al enviar email SendMailNormal {}:  {}", sMail, ex);
    }
    return flag;
  }

  public String EnviaMail(String Nombre, String Mail, String TipoMensaje, String Asunto,
      String Usuario, String Password, String Monto, String Autorizacion, String Comision,
      String Saldo) throws Exception {
    UsuarioDao dao = null;
    dao = (UsuarioDao) getBean(USUARIO_DAO);
    parametroVO p1 = new parametroVO();
    parametroVO p2 = new parametroVO();
    try {
      LOGGER.info("Asunto : " + Asunto);
      LOGGER.info("TipoMensaje :" + TipoMensaje);
      p1.setParam(Asunto);
      p2.setParam(TipoMensaje);
      java.util.Date fecha = new java.util.Date();

      LOGGER.debug("antes de obtener datos para mail ");
      String wAsunto = dao.consultaParametros(p1);
      LOGGER.info("wAsunto :" + wAsunto);
      String Cadena = dao.consultaParametros(p2);
      LOGGER.debug("despues de obtener datos para mail ");
      LOGGER.debug("wAsunto : " + wAsunto);
      LOGGER.debug("Cadena : " + Cadena);
      LOGGER.debug("Mail : " + Mail);
      Cadena = Cadena.replace("@NOMBRE", Nombre);
      Cadena = Cadena.replace("@USUARIO", Usuario);
      Cadena = Cadena.replace("@PASSWORD", Password);
      Cadena = Cadena.replace("@MONTO", Monto);
      Cadena = Cadena.replace("@AUTORIZACION", Autorizacion);
      Cadena = Cadena.replace("@COMISION", Comision);
      Cadena = Cadena.replace("@FECHA", fecha.toString());
      Cadena = Cadena.replace("@SALDO", Saldo);

      if (Cadena.indexOf("@CARGO") >= 0) {
        double Car = Double.parseDouble(Monto) + Double.parseDouble(Comision);
        Cadena = Cadena.replace("@CARGO", "" + Car);
      }

      LOGGER.debug("ENVIANDO MAIL");

      boolean x = sendMail(Mail, wAsunto, Cadena);

      if (x) {
        LOGGER.debug("EL MAIL SE ENVIO");
        return "0";

      } else {
        LOGGER.debug("EL MAIL NO SE ENVIO");
        return "-1";
      }
      // return "";
    } catch (Exception e) {
      LOGGER.error("EnviaMail: " + e.toString());
      return "-1";
    }
  }


  public boolean sendMail(String destinatario, String asunto, String cuerpo) throws Exception {
    UsuarioDao dao = null;
    dao = (UsuarioDao) getBean(USUARIO_DAO);
    parametroVO p1 = new parametroVO();
    parametroVO p2 = new parametroVO();
    parametroVO p3 = new parametroVO();
    parametroVO p4 = new parametroVO();
    parametroVO p5 = new parametroVO();
    parametroVO p6 = new parametroVO();
    try {
      p1.setParam("@SMTP");
      p2.setParam("@SMTP_PORT");
      p3.setParam("@SMTP_USER");
      p4.setParam("@SMTP_PWD");
      p5.setParam("@SMTP_MAIL_SEND");
      HOST = dao.consultaParametros(p1);
      LOGGER.debug("HOST : " + HOST);
      PORT = Integer.parseInt(dao.consultaParametros(p2));
      LOGGER.debug("PORT : " + PORT);
      USERNAME = dao.consultaParametros(p3);
      LOGGER.debug("USERNAME : " + USERNAME);
      PASSWORD = dao.consultaParametros(p4);
      LOGGER.debug("PASSWORD : " + PASSWORD);
      FROM = dao.consultaParametros(p5);
      LOGGER.debug("FROM : " + FROM);
      PROPS = new Properties();
      PROPS.put("mail.smtp.auth", "true");
      PROPS.put("mail.smtp.starttls.enable", "true");
      Session session = Session.getInstance(PROPS);

      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(FROM));
      // message.setFrom(new InternetAddress( "addcel99@gmail.com" ));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
      message.setSubject(asunto);

      if (cuerpo.indexOf("cid:identifierCID00") > 0) {
        // LOGGER.debug("Se agrega imagen: " + URL_HEADER_ADDCEL);
        BodyPart messageBodyPart = new MimeBodyPart();

        // Set the HTML content, be sure it references the attachment
        // String htmlText = "<H1>Hello</H1>" + "<img src=\"cid:memememe\">";

        // Set the content of the body part
        messageBodyPart.setContent(cuerpo, "text/html");

        // Create a related multi-part to combine the parts
        MimeMultipart multipart = new MimeMultipart("related");

        // Add body part to multipart
        multipart.addBodyPart(messageBodyPart);

        // Create part for the image
        messageBodyPart = new MimeBodyPart();

        // Fetch the image and associate to part
        DataSource fds = new FileDataSource("");
        messageBodyPart.setDataHandler(new DataHandler(fds));

        // Add a header to connect to the HTML
        messageBodyPart.setHeader("Content-ID", "identifierCID00");

        // Add part to multi-part
        multipart.addBodyPart(messageBodyPart);

        // Associate multi-part with message
        message.setContent(multipart);
      } else {
        // LOGGER.debug("Se agrega contenido HTML sin imagen...");
        // Create your new message part
        message.setContent(cuerpo, "text/html");
      }

      Transport transport = session.getTransport("smtp");
      transport.connect(HOST, PORT, USERNAME, PASSWORD);
      message.saveChanges();
      // Transport.send(message);
      transport.sendMessage(message, message.getAllRecipients());

      LOGGER.debug("Correo enviado exitosamente: " + destinatario);

      return true;

    } catch (Exception e) {
      LOGGER.error("ERROR - SEND MAIL: " + e);
      return false;
    }
  }


  /* Metodo para enviar correos de addcel y gmail */
  public void enviarCorreo(CorreoVO correo) {
	    properties.put("mail.smtp.host", "localhost");
	    properties.put("mail.smtp.starttls.enable", "true");
	    properties.put("mail.smtp.port", 25);
	    properties.put("mail.smtp.mail.sender", "mobilecard@addcel.com");
	    properties.put("mail.smtp.user", "mobilecard");
	    properties.put("mail.smtp.auth", "true");

	    session = Session.getDefaultInstance(properties);
	    try {
	      MimeMessage message = new MimeMessage(session);
	      message.setFrom(new InternetAddress((String) properties.get("mail.smtp.mail.sender")));
	      LOGGER.info("correo de usuario : " + correo.getTo()[0]);
	      message.addRecipient(Message.RecipientType.TO, new InternetAddress(correo.getTo()[0]));
	      message.setSubject(correo.getSubject());
	      message.setText(correo.getBody());
	      Transport t = session.getTransport("smtp");
	      t.connect((String) properties.get("mail.smtp.user"), "A66C3lm");
	      DataHandler dh = new DataHandler(correo.getBody(), "text/html");
	      message.setDataHandler(dh);
	      message.saveChanges();
	      t.sendMessage(message, message.getAllRecipients());
	      t.close();
	    } catch (MessagingException me) {
	      // Aqui se deberia o mostrar un mensaje de error o en lugar
	      // de no hacer nada con la excepcion, lanzarla para que el modulo
	      // superior la capture y avise al usuario con un popup, por ejemplo.
	      LOGGER.equals("ERROR : " + me.getMessage());
	      return;
	    }

  }

  public static void enviarCorreoMicrosoft(CorreoVO correo) throws Exception {

    // objeto donde almacenamos la configuración para conectarnos al servidor
    Properties properties = new Properties();
    // cargamos el archivo de configuracion
    // properties.load(new FileInputStream("/WEB-INF/properties/hotmail.properties"));
    properties.put("mail.smtp.host", "smtp.live.com");
    properties.put("mail.smtp.auth", "true");
    properties.put("mail.smtp.port", 25);
    properties.put("mail.smtp.starttls.enable", "true");
    properties.put("mail.from", "mobilecardusa@hotmail.com");
    properties.put("mail.user", "mobilecardusa@hotmail.com");
    properties.put("mail.password", "addcel2016");
    // creamos una sesión
    Session session = Session.getInstance(properties, null);
    // creamos el mensaje a enviar
    Message mensaje = new MimeMessage(session);
    // agregamos la dirección que envía el email
    mensaje.setFrom(new InternetAddress(properties.getProperty("mail.from")));
    String email = correo.getTo()[0];
    try {
      // agregamos las direcciones de email que reciben el email, en el primer parametro envíamos el
      // tipo de receptor
      mensaje.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
      LOGGER.info("Debug 7");
      // Message.RecipientType.TO; para
      // Message.RecipientType.CC; con copia
      // Message.RecipientType.BCC; con copia oculta
    } catch (Exception ex) {
      // en caso que el email esté mal formado lanzará una exception y la ignoramos
    }
    mensaje.setSubject(correo.getSubject());
    // agregamos una fecha al email
    mensaje.setSentDate(new Date(1, 1, 1));
    BodyPart messageBodyPart = new MimeBodyPart();
    messageBodyPart.setText(correo.getBody());
    SMTPTransport transport = (SMTPTransport) session.getTransport("smtp");
    try {
      // conectar al servidor
      transport.connect(properties.getProperty("mail.user"),
          properties.getProperty("mail.password"));
      // enviar el mensaje
      DataHandler dh = new DataHandler(correo.getBody(), "text/html");
      mensaje.setDataHandler(dh);
      mensaje.saveChanges();
      transport.sendMessage(mensaje, mensaje.getAllRecipients());
    } finally {
      // cerrar la conexión
      transport.close();
    }
  }
  
  
  /**
   * Tarjeta que sustituye los digitos correspondientes a los indices 4 - 11 con un caracter
   * cualquiera.
   * Si un caracter en cuestion se encuentra antes de un indice multiplo de 4 agrega un espacio a
   * la
   * representacion
   *
   * @param card - El numero de tarjeta a enmascarar
   * @param placeholder - Caracter a utilizar como sustitucion
   * @return String representando la tarjeta enmascarada
   */
  public String maskCard(String card, String placeholder) {
    String result = "";
    LOGGER.debug("Longitud tarjeta: {}", card.length());
    for (int i = 0; i < card.length(); i++) {
      if (i >= 4 && i <= 11) {
        if ((i + 1) % 4 == 0) {
          result += (placeholder + " ");
        } else {
          result += placeholder;
        }
      } else {
        if ((i + 1) % 4 == 0) {
          result += (card.charAt(i) + " ");
        } else {
          result += card.charAt(i);
        }
      }
    }
    return result;
  }  
}
