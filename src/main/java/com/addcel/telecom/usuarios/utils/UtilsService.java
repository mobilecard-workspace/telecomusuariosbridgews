package com.addcel.telecom.usuarios.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.addcel.telecom.usuarios.utils.Constantes;
import com.addcel.telecom.usuarios.ws.servicios.vo.UsuarioResponse;
import com.google.gson.Gson;
import crypto.Crypto;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UtilsService {
	
    private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
    private static final String patron = "dd/MM/yyyy";
	
    private static final SimpleDateFormat formato = new SimpleDateFormat(patron);

    private static final String TelefonoServicio = "5525963513";
    
    private static final String urlString = "http://192.168.75.53:8080/MailSenderAddcel/enviaCorreoAddcel";  
//    private static final String urlString = "http://199.231.161.38:8080/MailSenderAddcel/enviaCorreoAddcel";


    public static String setSMS(String Telefono) {
        return Crypto.aesEncrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String getSMS(String Telefono) {
        return Crypto.aesDecrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String parsePass(String pass) {
        int len = pass.length();
        String key = "";

        for (int i = 0; i < 32 / len; i++) {
            key += pass;
        }

        int carry = 0;
        while (key.length() < 32) {
            key += pass.charAt(carry);
            carry++;
        }
        return key;
    }
    
    public static String cambiarFormatoFecha(String fecha, String formatoEntrada, String formatoSalida){		
		String fechafmt = null;
		try {
			if(fecha != null && !fecha.equals("")){
				SimpleDateFormat sdfEntrada = new SimpleDateFormat(formatoEntrada);
				SimpleDateFormat sdfSalida = new SimpleDateFormat(formatoSalida);
				fechafmt = sdfSalida.format(sdfEntrada.parse(fecha));
			}		
		} catch (ParseException e) {			
			logger.error("Error cambiarFormatoFecha: {}", e);
		}				
		return fechafmt;
	}
	
	public static Date getFechaExpDate(String fechaExp){
		Date fecha = null;
		String[] fechaArr = null; 
		try{
			fechaArr = fechaExp.split("/");
			logger.info("Calculando fecha Expiracion: " + fechaExp);
			fecha = formato.parse(new StringBuffer().append("01/").append(fechaArr[0])
					.append("/20").append(fechaArr[1]).toString());
		}catch(Exception e){
			logger.error("Error en Fecha Expiracion: {}",e );
		}
		return fecha;
	}
    
	public static synchronized int diferenciasDeFechas(Date fechaInicial, Date fechaFinal) {
        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (ParseException ex) {
        }

        String fechaFinalString = df.format(fechaFinal);
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (ParseException ex) {
        }
        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return ((int) dias);
    }
	
	
	public static boolean isEmpty(String cadena){
		boolean resp = false;
		if(cadena == null){
			resp = true;
		}else if("".equals(cadena)){
			resp = true;
		}
		return resp; 
		
	}
	
	private static final String ERROR_100 = "Ocurrio un error general.";
	private static final String ERROR_101 = "Ocurrio un error general en el llamdo a la BD.";
	
	public static String getError(int idError, Exception e){
		Gson gson = new Gson();
		UsuarioResponse respuesta = new UsuarioResponse();
		respuesta.setIdError(idError);
		switch(idError){
			case 100:
				logger.error(ERROR_100, e);
				respuesta.setMensajeError(ERROR_100);
				break;
			case 101:
				logger.error(ERROR_101, e);
				respuesta.setMensajeError(ERROR_101);
				break;
		}
		return gson.toJson(respuesta);
	}
	
	private static final String BASE = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public static String generatePassword(int longitud){
		StringBuffer contrasena = new StringBuffer();
		int numero = 0;
		for(int i = 0; i < longitud; i++){ //1
		    numero = (int)(Math.random()*(BASE.length()-1)); //2
		    contrasena.append(BASE.substring(numero, numero+1)); //4
		}
		return contrasena.toString();
	}
        
        public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email Microsoft. ");
			URL url = new URL(Constantes.urlStringMail);
			logger.info("Conectando con " + Constantes.urlStringMail);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        
                        Authenticator au = new Authenticator() {
                        @Override
                        protected PasswordAuthentication
                           getPasswordAuthentication() {
                           return new PasswordAuthentication
                              ("jboss", "0pqbyK2bHXayJAR2".toCharArray());
                            }
                         };
                        Authenticator.setDefault(au);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor(envio email Microsoft) " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
        
        public static boolean patternMatches(String email, String pattern){
    	boolean flag = false;
    	
    	Pattern pat = Pattern.compile(pattern); //".*@(hotmail|live|msn|outlook)\\..*"
	     Matcher mat = pat.matcher(email.toLowerCase());
	     if (mat.matches()) {
	    	 flag = true;
	     }
    	return flag;
    }
    
        public static void MailSender(String data) {
    		String line = null;
    		StringBuilder sb = new StringBuilder();
    		try {
    			logger.info("Iniciando proceso de envio email Microsoft. ");
    			URL url = new URL(urlString);
    			logger.info("Conectando con " + urlString);
    			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

    			urlConnection.setDoOutput(true);
    			urlConnection.setRequestProperty("Content-Type", "application/json");
    			urlConnection.setRequestProperty("Accept", "application/json");
    			urlConnection.setRequestMethod("POST");

    			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
    			writter.write(data);
    			writter.flush();

    			logger.info("Datos enviados, esperando respuesta");

    			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

    			while ((line = reader.readLine()) != null) {
    				sb.append(line);
    			}

    			logger.info("Respuesta del servidor(envio email Microsoft) " + sb.toString());
    		} catch (Exception ex) {
    			logger.error("Error en: sendMail, al enviar el email ", ex);
    		}
    	}
        
        
}
