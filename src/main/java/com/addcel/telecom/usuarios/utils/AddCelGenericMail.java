package com.addcel.telecom.usuarios.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.telecom.usuarios.model.vo.CorreoVO;
import com.addcel.telecom.usuarios.model.vo.DatosCorreoVO;
import com.google.gson.Gson;
import java.net.Authenticator;
import java.net.PasswordAuthentication;


public class AddCelGenericMail {
	
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String urlString = "http://199.231.161.36:8080/MailSenderAddcel/enviaCorreoAddcel";
		
	private static final DecimalFormat df = new DecimalFormat("#,###,##0.00");
    	
	public static boolean sendMail(String subject,String body, String email) {
		String json = null;
		String from = null;
		try {
			CorreoVO correo = new CorreoVO();
			from = "no-reply@addcel.com";
			String[]to = {email};
			correo.setCc(new String[]{});
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			Gson gson = new Gson();
			json = gson.toJson(correo);
			sendMail(json);
			logger.info("Fin proceso de envio email");
			logger.debug("Correo enviado exitosamente: " + email);
			return true;
		} catch (Exception e) {
			logger.error("ERROR - SEND MAIL: " + e);
			return false;
		}
	}
	
	public static CorreoVO generatedMailIave(DatosCorreoVO datosCorreoVO, String body, String subject){
		logger.info("Genera objeto para envio de mail: " + datosCorreoVO.getEmail());
		String json = null;
		CorreoVO correo = new CorreoVO();
		try{
			body = body.toString();
			body = body.replace("@NOMBRE", datosCorreoVO.getNombre());
			body = body.replace("@USUARIO", datosCorreoVO.getUsuario());
			body = body.replace("@PASSWORD", "");
			body = body.replace("@MONTO", String.valueOf(datosCorreoVO.getMonto()));
			body = body.replace("@AUTORIZACION_IAVE", datosCorreoVO.getAutorizacionIave());
			body = body.replace("@AUTORIZACION", datosCorreoVO.getAutorizacion());
			body = body.replace("@COMISION", String.valueOf(datosCorreoVO.getComision()));
			body = body.replace("@FECHA", datosCorreoVO.getFecha());
			body = body.replace("@SALDO", "");
			body = body.replace("@TAG_IAVE", datosCorreoVO.getTagIave());
			if (body.indexOf("@CARGO") >= 0) {
				double Car = datosCorreoVO.getMonto() + datosCorreoVO.getComision();
				body = body.replace("@CARGO", "" + Car);
			}

			if (body.indexOf("@REFERENCIA") >= 0) {
				body = body.replace("@REFERENCIA", String.valueOf(datosCorreoVO.getIdBitacora()));
			}
			
			
			String from = "no-reply@addcel.com";
			String[] to = {datosCorreoVO.getEmail()};
			correo.setCc(new String[]{});
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			Gson gson = new Gson();
			json = gson.toJson(correo);
			sendMail(json);
			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static CorreoVO generatedMail(DatosCorreoVO datosCorreoVO, String body){
		logger.info("Genera objeto para envio de mail: " + datosCorreoVO.getEmail());
		String json = null;
		CorreoVO correo = new CorreoVO();
		try{
			body = body.replaceAll("<#NOMBRE#>", datosCorreoVO.getNombre());
			body = body.replaceAll("<#CONCEPTO#>",datosCorreoVO.getConcepto());
			body = body.replaceAll("<#PRODUCTO#>", "Pago de servicios "+datosCorreoVO.getDescServicio());
			body = body.replaceAll("<#FECHA#>", datosCorreoVO.getFecha());
			body = body.replaceAll("<#REFE#>", datosCorreoVO.getReferenciaServicio());
			body = body.replaceAll("<#FOLIO#>", String.valueOf(datosCorreoVO.getIdBitacora()));
			body = body.replaceAll("<#AUTBAN#>", datosCorreoVO.getNoAutorizacion()!= null ? datosCorreoVO.getNoAutorizacion() : "");
			body = body.replaceAll("<#FOLIOXCD#>", datosCorreoVO.getFolioXcd());
			body = body.replaceAll("<#IMPORTE#>", "\\$ "+df.format(datosCorreoVO.getImporte()));
			body = body.replaceAll("<#COMISION#>", "\\$ "+df.format(datosCorreoVO.getComision()));
			body = body.replaceAll("<#MONTO#>", "\\$ "+df.format(datosCorreoVO.getMonto()));
			body = body.replaceAll("<#MONEDA#>", "MXN");
			
			body = body.replaceAll("<#SUCU#>", "Mobilecard");
			body = body.replaceAll("<#CAJA#>", "1");
			body = body.replaceAll("<#CAJERO#>", "Autoservicio");
			body = body.replaceAll("<#FORMAPAGO#>", "Tarjeta de Credito");
			
			body = body.replaceAll("<#TICKET#>",datosCorreoVO.getTicket());
			
			String from = "no-reply@addcel.com";
			String subject =
					("Acuse de pago de ")	
					+ datosCorreoVO.getDescServicio() + " - ReferenciaMC: " + datosCorreoVO.getReferenciaServicio();
			String[] to = {datosCorreoVO.getEmail()};
			correo.setCc(new String[]{});
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			Gson gson = new Gson();
			json = gson.toJson(correo);
			sendMail(json);
			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}

	public static CorreoVO generatedMailCommons(DatosCorreoVO datosCorreoVO,
			String body, Properties props, String from, String host, int port,
			String username, String password) {
		logger.info("Genera objeto para envio de mail: "
				+ datosCorreoVO.getEmail());
		String json = null;
		CorreoVO correo = new CorreoVO();
		try {
			body = body.replaceAll("<#NOMBRE#>", datosCorreoVO.getNombre());
			body = body.replaceAll("<#CONCEPTO#>", datosCorreoVO.getConcepto());
			body = body.replaceAll("<#PRODUCTO#>", "Pago de servicios "
					+ datosCorreoVO.getDescServicio());
			body = body.replaceAll("<#FECHA#>", datosCorreoVO.getFecha());
			body = body.replaceAll("<#REFE#>",
					datosCorreoVO.getReferenciaServicio());
			body = body.replaceAll("<#FOLIO#>",
					String.valueOf(datosCorreoVO.getIdBitacora()));
			body = body.replaceAll(
					"<#AUTBAN#>",
					datosCorreoVO.getNoAutorizacion() != null ? datosCorreoVO
							.getNoAutorizacion() : "");
			body = body.replaceAll("<#FOLIOXCD#>", datosCorreoVO.getFolioXcd());
			body = body.replaceAll("<#IMPORTE#>",
					"\\$ " + df.format(datosCorreoVO.getImporte()));
			body = body.replaceAll("<#COMISION#>",
					"\\$ " + df.format(datosCorreoVO.getComision()));
			body = body.replaceAll("<#MONTO#>",
					"\\$ " + df.format(datosCorreoVO.getMonto()));
			body = body.replaceAll("<#MONEDA#>", "MXN");

			body = body.replaceAll("<#SUCU#>", "Mobilecard");
			body = body.replaceAll("<#CAJA#>", "1");
			body = body.replaceAll("<#CAJERO#>", "Autoservicio");
			body = body.replaceAll("<#FORMAPAGO#>", "Tarjeta de Credito");

			body = body.replaceAll("<#TICKET#>", datosCorreoVO.getTicket());

			String subject = ("Acuse de pago de ")
					+ datosCorreoVO.getDescServicio() + " - ReferenciaMC: "
					+ datosCorreoVO.getReferenciaServicio();
			//Utils.sendMail(datosCorreoVO.getEmail(), subject, body, props, from, host, port, username, password);
			logger.info("Fin proceso de envio email");
		} catch (Exception e) {
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}
	
	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        Authenticator au = new Authenticator() {
                        @Override
                        protected PasswordAuthentication
                           getPasswordAuthentication() {
                           return new PasswordAuthentication
                              ("admin", "0pqbyK2bHXayJAR2".toCharArray());
                            }
                         };
                        Authenticator.setDefault(au);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
